package coder.reprodutiva;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.data.SharedPref;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.Tools;

public class ActivityEditUsername extends AppCompatActivity {
    EditText nama;
    private SharedPref sharedPref;
    SharedPreferences userPref;
    TextView cancelStatus,OkStatus;
    private Toolbar toolbar;
    private ActionBar actionBar;
    ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_username);
        userPref = PreferenceManager.getDefaultSharedPreferences(ActivityEditUsername.this);
        sharedPref = new SharedPref(this);

        cancelStatus = findViewById(R.id.cancelStatus);
        OkStatus = findViewById(R.id.OkStatus);
        OkStatus.setOnClickListener(view -> gantiNama());
        cancelStatus.setOnClickListener(v -> {
            Intent mIntent = new Intent(this, ActivityProfile.class);
            startActivity(mIntent);
        });
        nama = findViewById(R.id.nama);
        nama.setText(userPref.getString("nama", ""));
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Edit Display Name");
        actionBar.setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(ActivityEditUsername.this);

    }
    private void gantiNama() {
        String namana = nama.getText().toString().trim();
        if (namana.isEmpty()) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_nama, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }else{
            hideKeyboard();
            do_ganti(namana);
        }
    }
    private void do_ganti(final String namana) {
        progressDialog.setMessage("Please wait...");
        showDialog();
        new Handler().postDelayed(() -> submitNama(namana), 2000);
    }
    private void submitNama(final String namana) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            final String no_hp = userPref.getString("no_hp", "");
            String tag_string_req = "req_ganti_nama";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_GANTI_NAMA, response -> {
                        hideDialog();
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {

                                SharedPreferences.Editor namax = userPref.edit();
                                namax.putString("nama",namana);
                                namax.commit();

                                Intent mIntent = new Intent(this, ActivityProfile.class);
                                startActivity(mIntent);


                            } else {
                                dialogGagal();
                            }
                        } catch (JSONException e) {
                            Log.e("Data","NGanu : " + e);
                        }
                    }, error -> {
                hideDialog();
                dialogNoInternet();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("nama",namana);
                    params.put("no_hp",no_hp);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_nama, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        super.onBackPressed();
    }
}
