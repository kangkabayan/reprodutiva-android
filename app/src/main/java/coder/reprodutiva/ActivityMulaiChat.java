package coder.reprodutiva;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import coder.reprodutiva.adapter.ChatAdminAdapter;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.LoaderView;
import coder.reprodutiva.utils.NetworkCheck;

public class ActivityMulaiChat extends AppCompatActivity {
    String no_hp_admin,nama_admin;
    RecyclerView recyclerView;
    SharedPreferences userPref;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private boolean isGroup;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager manager;
    Socket socket;
    LoaderView loader;
    ImageButton kirim;
    private ImageButton smileyButton;
    private EmojiEditText chat;
    private EmojiPopup emojiPopup;
    private final int OPEN_IMAGE = 536;
    public static final int REQUEST_IMAGE = 100;
    ArrayList<String> chat_data=new ArrayList<>();
    ArrayList<String> pengirim_=new ArrayList<>();
    ArrayList<String> foto_pengirim_=new ArrayList<>();
    ArrayList<String> jam_kirim_ =new ArrayList<>();
    ArrayList<String> saha_ =new ArrayList<>();
    ArrayList<String> image_ =new ArrayList<>();
    ArrayList<String> no_tujuan_ =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mulai_chat);
        userPref = android.preference.PreferenceManager.getDefaultSharedPreferences(ActivityMulaiChat.this);
        View parent_view = findViewById(android.R.id.content);
        setupToolbar();
        initComponent();

    }
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        Bundle b = getIntent().getExtras();
        assert b != null;
        no_hp_admin = b.getString("no_hp_admin");
        nama_admin = b.getString("nama_admin");
        actionBar.setTitle(nama_admin);
    }
    private void initComponent() {
        recyclerView = findViewById(R.id.list_chat_private);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        adapter= new ChatAdminAdapter(this,chat_data,pengirim_,foto_pengirim_,jam_kirim_,saha_,image_,no_tujuan_);
        recyclerView.setAdapter(adapter);
//        manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
        smileyButton = findViewById(R.id.button_smiley);
        chat = findViewById(R.id.input_text_chat);
        kirim = findViewById(R.id.button_send);
        kirim.setOnClickListener(view -> {
            if (!NetworkCheck.isConnect(this)) {
                dialogNoInternet();
            } else {
                kirim_pesan();
                chat.setText("");
            }
        });
        emojiPopup = EmojiPopup.Builder.fromRootView(getWindow().getDecorView().findViewById(android.R.id.content))
                .setOnEmojiPopupShownListener(() -> smileyButton.setImageResource(R.drawable.ic_keyboard)).setOnEmojiPopupDismissListener(() -> smileyButton.setImageResource(R.drawable.ic_smiley)).setOnSoftKeyboardCloseListener(() -> emojiPopup.dismiss()).build(chat);

        smileyButton.setOnClickListener(view -> emojiPopup.toggle());
        findViewById(R.id.button_add_image).setOnClickListener(view -> {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            openImageBottomSheet();
        });
        try{
            socket= IO.socket(Constant.CHAT_SERVER_URL);
        }
        catch (URISyntaxException e){
            Log.d("error","onCreate"+e.toString());
        }
        socket.connect();
        socket.on("pesan_admin",handling);
//        fetchChatThread();
    }
    private void openImageBottomSheet() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, OPEN_IMAGE);
                return;
            }
        }
        showImagePickerOptions();
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        if (!isFinishing()) {
            dialog.show();
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            int length = charSequence.length();
            kirim.setVisibility(length > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    @Override
    public void onStart() {
        super.onStart();
        chat.addTextChangedListener(textWatcher);

    }
    private Emitter.Listener handling = args -> this.runOnUiThread(() -> tambahPesanAdmin(args[0].toString(),args[1].toString(),args[2].toString(),args[3].toString(),args[4].toString(),args[5].toString(),args[6].toString()));
    public void kirim_pesan(){
        final String nama = userPref.getString("nama", "");
        final String foto = userPref.getString("foto", "");
        final String no_hpna = userPref.getString("no_hp", "");

        String pesan = chat.getText().toString().trim();
        chat.setText("");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
        String jam_kirim = mdformat.format(calendar.getTime());
        String tag_string_req = "req_kirim_chat";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Constant.URL_CHAT, response -> {
            try {
                JSONObject jObj = new JSONObject(response);
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    if(!isGroup && no_hpna == no_hpna || no_hp_admin == no_hpna){
                        tambahPesanAdmin(pesan,nama,foto,jam_kirim,no_hpna,"no.jpg",no_hp_admin);
                        socket.emit("pesan_admin",pesan,nama,foto,jam_kirim,no_hpna,"no.jpg",no_hp_admin);
                    }
                } else {
                    dialogGagal();
                }
            } catch (JSONException e) {
                Log.e("Data","Nganu : " + e);
            }
        }, error -> dialogNoInternet()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("isi_pesan",pesan);
                params.put("no_hp",no_hpna);
                params.put("no_hp_tujuan",no_hp_admin);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_kirim_pesan, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void tambahPesanAdmin(String pesan, String pengirim, String foto, String jam_kirim, String saha, String image, String no_tujuan_) {
        chat_data.add(pesan);
        this.pengirim_.add(pengirim);
        this.foto_pengirim_.add(foto);
        this.jam_kirim_.add(jam_kirim);
        this.saha_.add(saha);
        this.image_.add(image);
        this.no_tujuan_.add(no_tujuan_);
//        adapter.notifyItemInserted(chat_data.size()-1);
//        recyclerView.smoothScrollToPosition(chat_data.size()-1);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void fetchChatThread() {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            final String no_hpna = userPref.getString("no_hp", "");

            String tag_string_req = "req_chat_private";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_GET_PRIVATE_CHAT, response -> {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONArray commentsObj = jObj.getJSONArray("data");

                        for (int i = 0; i < commentsObj.length(); i++) {
                            JSONObject commentObj = (JSONObject) commentsObj.get(i);

                            String no_hp = commentObj.getString("no_hp");
                            String nama = commentObj.getString("nama");
                            String tgl = commentObj.getString("tgl");
                            String pesan = commentObj.getString("pesan");
                            String foto = commentObj.getString("foto");
                            String image = commentObj.getString("foto_media");
                            String no_tujuan = commentObj.getString("no_hp_tujuan");

                            chat_data.add(pesan);
                            pengirim_.add(nama);
                            foto_pengirim_.add(foto);
                            jam_kirim_.add(tgl);
                            saha_.add(no_hp);
                            image_.add(image);
                            no_tujuan_.add(no_tujuan);

                        }

                        adapter.notifyDataSetChanged();
                        if (adapter.getItemCount() > 1) {
                            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, adapter.getItemCount() - 1);
                        }

                    }
                } catch (JSONException e) {
                    Log.e("Data","Nganu : " + e);
                }
            }, error -> dialogNoInternet()) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("no_hp",no_hpna);
                    params.put("no_hp_tujuan",no_hp_admin);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void uploadServer(final String lokasi_foto) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            final String nama = userPref.getString("nama", "");
            final String foto = userPref.getString("foto", "");
            final String no_hpna = userPref.getString("no_hp", "");
            String pesan = chat.getText().toString().trim();
            chat.setText("");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
            String jam_kirim = mdformat.format(calendar.getTime());
            Random r = new Random();
            int randomNumber = r.nextInt(555555555);
            String tag_string_req = "req_ganti_upload_foto";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_UPLOAD_FOTO_PRIVATE, response -> {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        if(!isGroup && no_hpna == no_hpna || no_hp_admin == no_hpna){
                            tambahPesanAdmin("upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg",no_hp_admin);
                            socket.emit("pesan_admin","upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg",no_hp_admin);
                        }

                    } else {
                        dialogGagal();
                    }
                } catch (JSONException e) {
                    Log.e("Data","Nganu : " + e);
                    dialogGagal();
                }
            }, error -> {
                dialogNoInternet();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("foto",lokasi_foto);
                    params.put("no_hp",no_hpna);
                    params.put("no_hp_tujuan",no_hp_admin);
                    params.put("nama_file",String.valueOf(randomNumber));
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), uri);
                    new Handler().postDelayed(() -> getStringImage(bitmap), 2000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        uploadServer(encodedImage);
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.off("pesan_admin");
        socket.disconnect();
    }
    @Override
    public void onResume() {
        adapter.notifyDataSetChanged();
        super.onResume();
    }
    @Override
    public void onBackPressed() {
        onBackAction();
    }
    private void onBackAction() {
        Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
