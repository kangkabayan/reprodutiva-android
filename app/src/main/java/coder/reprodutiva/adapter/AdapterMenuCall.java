package coder.reprodutiva.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.R;
import coder.reprodutiva.data.AppConfig;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.MenuCall;
import coder.reprodutiva.utils.Tools;


public class AdapterMenuCall extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context ctx;
    private List<MenuCall> items;
    private boolean loading;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, MenuCall obj);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void insertData(List<MenuCall> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView brief;
        public TextView no_tlp;
        public ImageView image;
        public LinearLayout lyt_color;
        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            brief = v.findViewById(R.id.brief);
            no_tlp = v.findViewById(R.id.no_tlp);
            image = v.findViewById(R.id.image);
            lyt_color = v.findViewById(R.id.lyt_color);
            lyt_parent = v.findViewById(R.id.lyt_parent);
        }
    }

    public AdapterMenuCall(Context context, RecyclerView view, List<MenuCall> items) {
        this.items = items;
        ctx = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_call, parent, false);
        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder vItem = (ViewHolder) holder;
            final MenuCall c = items.get(position);
            vItem.name.setText(c.title);
            vItem.brief.setText(c.deskripsi);
            vItem.no_tlp.setText(c.no_tlp);
            vItem.lyt_color.setBackgroundColor(Color.parseColor(c.color));
            Tools.displayImageThumbnail(ctx, vItem.image, Constant.getURLimgMenu(c.icon), 0.5f);

            if (AppConfig.TINT_CATEGORY_ICON) {
                vItem.image.setColorFilter(Color.WHITE);
            }

            vItem.lyt_parent.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, c);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<MenuCall> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

}