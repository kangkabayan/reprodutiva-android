package coder.reprodutiva.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.facebook.drawee.view.SimpleDraweeView;
import com.vanniktech.emoji.EmojiTextView;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.R;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.DataAdmin;
import coder.reprodutiva.model.DataGroupChat;
import coder.reprodutiva.utils.Tools;


public class AdapterDataChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context ctx;
    private List<DataGroupChat> items;
    private boolean loading;

    private OnItemClickListener onItemClickListener;
    private OnLoadMoreListener onLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(View view, DataGroupChat obj);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void insertData(List<DataGroupChat> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView user,jam_kirim;
        EmojiTextView text_chat;
        SimpleDraweeView leftProfileImage;
        LinearLayout layout;

        public ViewHolder(View v) {
            super(v);
            text_chat = itemView.findViewById(R.id.left_text);
            jam_kirim = itemView.findViewById(R.id.left_text_time);
            user = itemView.findViewById(R.id.left_display_name);
            layout = itemView.findViewById(R.id.linear);
            leftProfileImage = itemView.findViewById(R.id.left_profile_pic);
        }
    }

    public AdapterDataChat(Context context, RecyclerView view, List<DataGroupChat> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_group, parent, false);
        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder vItem = (ViewHolder) holder;
            final DataGroupChat c = items.get(position);
            vItem.text_chat.setText(c.pesan);
            vItem.jam_kirim.setText(c.tgl);
            Tools.displayImageThumbnail(ctx, vItem.leftProfileImage, Constant.getURLimgMember(c.foto), 0.5f);

            vItem.layout.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, c);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<DataGroupChat> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }
    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.REQ_BERITA;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }
    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }
    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

}