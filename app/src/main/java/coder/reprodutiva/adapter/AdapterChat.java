package coder.reprodutiva.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.vanniktech.emoji.EmojiTextView;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.R;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.Chat;

public class AdapterChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String emo_regex = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    boolean isOneEmoji = false;
    private List<Chat> items;
    private final int VIEW_LEFT_TEXT = 1;

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Chat obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterChat(Context context, RecyclerView view, List<Chat> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView leftProfileImage;
        LinearLayout replyLayout;
        LinearLayout linear_u;
        TextView leftDisplayName;
        EmojiTextView leftText;
        TextView leftTime;

        public OriginalViewHolder(View v) {
            super(v);
            leftProfileImage = v.findViewById(R.id.left_profile_pic);
            leftDisplayName = itemView.findViewById(R.id.left_display_name);
            leftText = itemView.findViewById(R.id.left_text);
            leftTime = itemView.findViewById(R.id.left_text_time);
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {

            final Chat o = items.get(position);
            OriginalViewHolder vItem = (OriginalViewHolder) holder;

            RoundingParams circle = RoundingParams.asCircle()
                    .setRoundingMethod(RoundingParams.RoundingMethod.BITMAP_ONLY);
            vItem.leftProfileImage.getHierarchy().setRoundingParams(circle);
            vItem.leftProfileImage.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
            vItem.leftProfileImage.setImageURI(Constant.getURLimgMember(o.foto));
            vItem.leftTime.setText(o.tgl_add);
            vItem.leftDisplayName.setText(o.nama);
            boolean isOneEmoji = false;
            for (String word : o.pesan.split(" ")) {
                int length = o.pesan.length();
                if (word.matches(emo_regex) && length == 2) {
                    isOneEmoji = true;
                }
            }
            int normalSize = (int) ctx.getResources().getDimension(R.dimen.emoji_size);
            int largerSize = (int) ctx.getResources().getDimension(R.dimen.emoji_size_larger);
            vItem.leftText.setEmojiSize(isOneEmoji ? largerSize : normalSize);
            vItem.leftText.setText(o.pesan);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<Chat> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.REQ_BERITA;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}