package coder.reprodutiva.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.vanniktech.emoji.EmojiTextView;

import java.util.ArrayList;

import coder.reprodutiva.ActivityFullScreenFoto;
import coder.reprodutiva.ActivityInfoProfile;
import coder.reprodutiva.ActivityMulaiChat;
import coder.reprodutiva.R;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.fragment.ChatAdmin;

public class ChatAdminAdapter extends RecyclerView.Adapter<ChatAdminAdapter.ViewHolder> {
    ArrayList<String> chat_data;
    ArrayList<String> pengirim_;
    ArrayList<String> foto_pengirim_;
    ArrayList<String> jam_kirim_;
    ArrayList<String> saha_;
    ArrayList<String> image_;
    ArrayList<String> no_tujuan_;

    private final String emo_regex = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";
    Context context;
    SharedPreferences userPref;

    public ChatAdminAdapter(Context context, ArrayList<String> chat_data, ArrayList<String> pengirim_, ArrayList<String> foto_pengirim_, ArrayList<String> jam_kirim_, ArrayList<String> saha_, ArrayList<String> image_, ArrayList<String> no_tujuan_) {
        this.chat_data = chat_data;
        this.pengirim_ = pengirim_;
        this.foto_pengirim_ = foto_pengirim_;
        this.jam_kirim_ = jam_kirim_;
        this.saha_ = saha_;
        this.image_ = image_;
        this.no_tujuan_ = no_tujuan_;
        this.context = context;
    }

    @NonNull
    @Override
    public ChatAdminAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_admin,parent,false);
        userPref = PreferenceManager.getDefaultSharedPreferences(context);
        return new ViewHolder(v);
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public void onBindViewHolder(@NonNull ChatAdminAdapter.ViewHolder holder, int position) {
        final String no_hp_sess = userPref.getString("no_hp","");
        if(saha_.get(position).equals(no_hp_sess)){
            holder.getLayoutKanan().setVisibility(View.VISIBLE);
            holder.getLayout().setVisibility(View.GONE);
            /*Kanan*/
            if (chat_data.get(position).equals("upload_gambar")){
                holder.getImage().setVisibility(View.VISIBLE);
                holder.getText_chat_kanan().setVisibility(View.GONE);
                holder.getImage_kanan().setImageURI(Constant.getURLMedia(image_.get(position)));
                holder.getWaktu_kanan().setText(jam_kirim_.get(position));
                holder.getImage_kanan().setOnClickListener(view -> {
                    ArrayList<String> images_list = new ArrayList<>();
                    images_list.add(Constant.getURLMedia(image_.get(position)));
                    Intent i = new Intent(context, ActivityFullScreenFoto.class);
                    i.putStringArrayListExtra(ActivityFullScreenFoto.EXTRA_IMGS, images_list);
                    context.startActivity(i);
                });
            }else{
                holder.getImage().setVisibility(View.GONE);
                holder.getText_chat_kanan().setVisibility(View.VISIBLE);
                holder.getImage_kanan().setVisibility(View.GONE);
                holder.getText_chat_kanan().setText(chat_data.get(position));
                holder.getWaktu_kanan().setText(jam_kirim_.get(position));
            }
        }else{
            /*Kiri*/
            holder.getLayout().setVisibility(View.VISIBLE);
            holder.getLayoutKanan().setVisibility(View.GONE);
            if (chat_data.get(position).equals("upload_gambar")){
                holder.getImage().setVisibility(View.VISIBLE);
                holder.getText_chat().setVisibility(View.GONE);
                holder.getImage().setImageURI(Constant.getURLMedia(image_.get(position)));
                holder.getUser().setText(pengirim_.get(position));
                holder.getWaktu().setText(jam_kirim_.get(position));
                RoundingParams circle = RoundingParams.asCircle()
                        .setRoundingMethod(RoundingParams.RoundingMethod.BITMAP_ONLY);
                holder.getFoto().getHierarchy().setRoundingParams(circle);
                holder.getFoto().getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
                holder.getFoto().setImageURI(Constant.getURLimgMember(foto_pengirim_.get(position)));
                holder.getImage().setOnClickListener(view -> {
                    ArrayList<String> images_list = new ArrayList<>();
                    images_list.add(Constant.getURLMedia(image_.get(position)));
                    Intent i = new Intent(context, ActivityFullScreenFoto.class);
                    i.putStringArrayListExtra(ActivityFullScreenFoto.EXTRA_IMGS, images_list);
                    context.startActivity(i);
                });
                holder.getFoto().setOnClickListener(view -> {
                    Bundle b = new Bundle();
                    Intent i;
                    i = new Intent(context, ActivityInfoProfile.class);
                    b.putString("no_hp", saha_.get(position));
                    b.putString("foto_profile", foto_pengirim_.get(position));
                    b.putString("nama", pengirim_.get(position));
                    i.putExtras(b);
                    context.startActivity(i);
                });
                holder.getUser().setOnClickListener(view -> {
                    Bundle b = new Bundle();
                    Intent i;
                    i = new Intent(context, ActivityInfoProfile.class);
                    b.putString("no_hp", saha_.get(position));
                    b.putString("foto_profile", foto_pengirim_.get(position));
                    b.putString("nama", pengirim_.get(position));
                    i.putExtras(b);
                    context.startActivity(i);
                });
            }else{
                holder.getImage().setVisibility(View.GONE);
                holder.getText_chat().setVisibility(View.VISIBLE);
                holder.getText_chat().setText(chat_data.get(position));
                holder.getUser().setText(pengirim_.get(position));
                holder.getWaktu().setText(jam_kirim_.get(position));
                RoundingParams circle = RoundingParams.asCircle()
                        .setRoundingMethod(RoundingParams.RoundingMethod.BITMAP_ONLY);
                holder.getFoto().getHierarchy().setRoundingParams(circle);
                holder.getFoto().getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
                holder.getFoto().setImageURI(Constant.getURLimgMember(foto_pengirim_.get(position)));
                holder.getFoto().setOnClickListener(view -> {
                    Bundle b = new Bundle();
                    Intent i;
                    i = new Intent(context, ActivityInfoProfile.class);
                    b.putString("no_hp", saha_.get(position));
                    b.putString("foto_profile", foto_pengirim_.get(position));
                    b.putString("nama", pengirim_.get(position));
                    i.putExtras(b);
                    context.startActivity(i);
                });
                holder.getUser().setOnClickListener(view -> {
                    Bundle b = new Bundle();
                    Intent i;
                    i = new Intent(context, ActivityInfoProfile.class);
                    b.putString("no_hp", saha_.get(position));
                    b.putString("foto_profile", foto_pengirim_.get(position));
                    b.putString("nama", pengirim_.get(position));
                    i.putExtras(b);
                    context.startActivity(i);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return chat_data.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView user,jam_kirim,right_text_time;
        EmojiTextView text_chat,right_text;
        SimpleDraweeView leftProfileImage,left_image_high_res,right_image;
        LinearLayout layout,layout_kanan;
        public ViewHolder(View itemView) {
            super(itemView);
            text_chat = itemView.findViewById(R.id.left_text);
            jam_kirim = itemView.findViewById(R.id.left_text_time);
            user = itemView.findViewById(R.id.left_display_name);
            layout = itemView.findViewById(R.id.kiri);
            layout_kanan = itemView.findViewById(R.id.kanan);
            leftProfileImage = itemView.findViewById(R.id.left_profile_pic);
            left_image_high_res = itemView.findViewById(R.id.left_image_high_res);
            right_text_time = itemView.findViewById(R.id.right_text_time);
            right_text = itemView.findViewById(R.id.right_text);
            right_image = itemView.findViewById(R.id.right_image);
        }

        public TextView getText_chat() {
            return text_chat;
        }
        public TextView getText_chat_kanan() {
            return right_text;
        }

        public TextView getUser() {
            return user;
        }
        public TextView getWaktu() {
            return jam_kirim;
        }
        public TextView getWaktu_kanan() {
            return right_text_time;
        }

        public SimpleDraweeView getFoto() {
            return leftProfileImage;
        }
        public SimpleDraweeView getImage() {
            return left_image_high_res;
        }
        public SimpleDraweeView getImage_kanan() {
            return right_image;
        }

        public LinearLayout getLayout() {
            return layout;
        }
        public LinearLayout getLayoutKanan() {
            return layout_kanan;
        }
    }

}
