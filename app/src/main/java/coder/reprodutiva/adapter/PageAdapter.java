package coder.reprodutiva.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import java.util.List;

import coder.reprodutiva.fragment.ChatAdmin;
import coder.reprodutiva.fragment.ChatFragment;
import coder.reprodutiva.fragment.ChatPrivate;

public class PageAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    private List<ChatFragment> mFragments;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.NUM_ITEMS = numOfTabs;
    }
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChatFragment.newInstance();
            case 1:
                return ChatPrivate.newInstance();
            case 2:
                return ChatAdmin.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
    @Override
    public int getItemPosition(Object object) {
        Fragment target = (Fragment) object;
        if (mFragments.contains(target)) {
            return POSITION_UNCHANGED;
        }
        return POSITION_NONE;
    }
    public void refresh() {
        mFragments.clear();
        notifyDataSetChanged();
    }
}
