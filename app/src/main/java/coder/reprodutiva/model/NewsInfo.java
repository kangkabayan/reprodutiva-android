package coder.reprodutiva.model;

public class NewsInfo {
    public Long id;
    public String judul;
    public String berita;
    public String berita_singkat;
    public String foto;
    public String publish;
    public String tanggal;
}
