package coder.reprodutiva.model;

import java.io.Serializable;

public class MenuCall implements Serializable {

    public Long id;
    public String title;
    public String icon;
    public String deskripsi;
    public String no_tlp;
    public String color;
}
