package coder.reprodutiva.model;

import java.io.Serializable;

public class DataAdmin implements Serializable {

    public Long id;
    public String nama;
    public String foto;
    public String no_hp;
}
