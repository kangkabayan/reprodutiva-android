package coder.reprodutiva.model;

public class UserImage {
    private long timestamp;
    private String id;
    private String name;
    private String thumbPic;
    private String originalPic;
    private String token;
    private boolean mainImage;
    private boolean selected;

    public UserImage() {
    }

    public UserImage(String thumbPic, String originalPic) {
        this.thumbPic = thumbPic;
        this.originalPic = originalPic;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbPic() {
        return thumbPic;
    }

    public void setThumbPic(String thumbPic) {
        this.thumbPic = thumbPic;
    }

    public String getOriginalPic() {
        return originalPic;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setOriginalPic(String originalPic) {
        this.originalPic = originalPic;
    }

    public boolean isMainImage() {
        return mainImage;
    }

    public void setMainImage(boolean mainImage) {
        this.mainImage = mainImage;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
