package coder.reprodutiva;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.Notification;
import coder.reprodutiva.utils.Tools;

public class ActivityDialogNotification extends AppCompatActivity {

    private static final String EXTRA_OBJECT = "key.EXTRA_OBJECT";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    private static final String EXTRA_POSITION = "key.EXTRA_FROM_POSITION";

    // activity transition
    public static void navigate(Activity activity, Notification obj, Boolean from_notif, int position) {
        Intent i = navigateBase(activity, obj, from_notif);
        i.putExtra(EXTRA_POSITION, position);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, Notification obj, Boolean from_notif) {
        Intent i = new Intent(context, ActivityDialogNotification.class);
        i.putExtra(EXTRA_OBJECT, obj);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        return i;
    }

    private Boolean from_notif;
    private Notification notification;
    private Intent intent;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_notification);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        notification = (Notification) getIntent().getSerializableExtra(EXTRA_OBJECT);
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);
        position = getIntent().getIntExtra(EXTRA_POSITION, -1);

        // set notification as read
        notification.read = true;

        initComponent();
    }

    private void initComponent() {


        String image_url = null;
        if (notification.type.equals("NEWS_INFO")) {
            image_url = Constant.getURLberita(notification.image);
        } else if (notification.type.equals("GROUP_CHAT")) {
            image_url = Constant.getURLimgMember(notification.image);
        }

        if (notification.type.equals("NEWS_INFO")) {
            findViewById(R.id.data_chat).setVisibility(View.GONE);
            findViewById(R.id.data_artikel).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.nama_umkm)).setText(notification.title);
            ((TextView) findViewById(R.id.content)).setText(notification.content);
            ((TextView) findViewById(R.id.date)).setText(Tools.getFormattedDate(notification.created_at));
            findViewById(R.id.lyt_image).setVisibility(View.GONE);
            if (image_url != null) {
                findViewById(R.id.lyt_image).setVisibility(View.VISIBLE);
                Tools.displayImageOriginal(this, findViewById(R.id.image), image_url);
            } else if (!from_notif) {
                findViewById(R.id.bt_open).setVisibility(View.GONE);
            }
            if (from_notif) {
                if (image_url == null && MainActivity.active) {
                    findViewById(R.id.lyt_action).setVisibility(View.GONE);
                }
            } else {
                ((TextView) findViewById(R.id.dialog_title)).setText(getString(R.string.title_notif_details));
                findViewById(R.id.logo).setVisibility(View.GONE);
                findViewById(R.id.view_space).setVisibility(View.GONE);
            }
        } else if (notification.type.equals("GROUP_CHAT")) {
            ((TextView) findViewById(R.id.nama_member)).setText(notification.title);
            ((TextView) findViewById(R.id.isi_chat)).setText(notification.content);
            ((TextView) findViewById(R.id.tanggal)).setText(Tools.getFormattedDate(notification.created_at));
            findViewById(R.id.lyt_image).setVisibility(View.GONE);
            findViewById(R.id.data_chat).setVisibility(View.VISIBLE);
            findViewById(R.id.data_artikel).setVisibility(View.GONE);

            if (image_url != null) {
                Tools.displayImageOriginal(this, findViewById(R.id.profileImageView), image_url);
            } else if (!from_notif) {
                findViewById(R.id.bt_open).setVisibility(View.GONE);
            }

            if (from_notif) {
                if (image_url == null && MainActivity.active) {
                    findViewById(R.id.lyt_action).setVisibility(View.GONE);
                }
            } else {
                ((TextView) findViewById(R.id.dialog_title)).setText(getString(R.string.title_notif_details));
                findViewById(R.id.logo).setVisibility(View.GONE);
                findViewById(R.id.view_space).setVisibility(View.GONE);
            }
        }

        intent = new Intent(this, ActivitySplash.class);
        if (notification.type.equals("NEWS_INFO")) {
            intent = ActivityBeritaDetil.navigateBase(this, notification.obj_id, from_notif);
        } else if (notification.type.equals("GROUP_CHAT")) {
            intent = new Intent(this, ActivityChat.class);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        findViewById(R.id.img_close).setOnClickListener(view -> finish());

        findViewById(R.id.bt_open).setOnClickListener(view -> {
            finish();
            startActivity(intent);
        });
    }
}
