package coder.reprodutiva;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;

import com.google.android.material.snackbar.Snackbar;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.data.SharedPref;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.NetworkCheck;

public class ActivityLoginHp extends AppCompatActivity implements View.OnClickListener {

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private PinView pinView;
    private Button next;
    private TextView topText,textU;
    private EditText userPhone,nama;
    private ConstraintLayout first, second, tiga;
    private int counter = 60;
    TextView textView;
    private static ConstraintLayout loginLayout,profileLayout,otpLayout;
    private static Animation shakeAnimation;
    CountryCodePicker ccp;
    ProgressDialog progressDialog = null;
    SharedPreferences userPref;
    private SharedPref sharedPref;
    private Spinner jns_kel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_hp);
        userPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref = new SharedPref(this);

        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        textView = findViewById(R.id.txt_timer);
        shakeAnimation = AnimationUtils.loadAnimation(this,
                R.anim.shake);
        loginLayout = findViewById(R.id.first_step);
        profileLayout = findViewById(R.id.StepTiga);
        otpLayout= findViewById(R.id.secondStep);

        ccp = findViewById(R.id.ccp);
        topText = findViewById(R.id.topText);
        pinView = findViewById(R.id.pinView);
        next = findViewById(R.id.button);
        userPhone = findViewById(R.id.userPhone);
        first = findViewById(R.id.first_step);

        second = findViewById(R.id.secondStep);
        textU = findViewById(R.id.textView_noti);
        first.setVisibility(View.VISIBLE);
        next.setOnClickListener(this);

        tiga = findViewById(R.id.StepTiga);
        nama = findViewById(R.id.nama);
        jns_kel = findViewById(R.id.jns_kel);

        mCallbacks =new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                progressDialog.setTitle("Kodigu OTP..");
                progressDialog.setMessage("Favor hein prosesu haruka kodigu OTP");
                progressDialog.show();
                new Handler().postDelayed(() -> signInWithPhoneAuthCredential(phoneAuthCredential), 500);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                pinView.setLineColor(Color.RED);
                textU.setText("X Incorrect OTP");
                textU.setTextColor(Color.RED);
            }
            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        hideDialog();
                        progressDialog.setTitle("Verifikasaun OTP susesu ona...");
                        progressDialog.setMessage("Favor hein uituan...");
                        progressDialog.show();

                        new Handler().postDelayed(() -> {
                            hideDialog();
                            next.setText("OK");
                            first.setVisibility(View.GONE);
                            second.setVisibility(View.GONE);
                            tiga.setVisibility(View.VISIBLE);
                            topText.setText("Kompletu o nia informasaun !");

                        }, 1000);
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            hideDialog();
                            pinView.setLineColor(Color.RED);
                            textU.setText("X Kodigu OTP Invalidu");
                            textU.setTextColor(Color.RED);
                            pinView.setText("");
                        }
                    }
                });
    }
    @Override
    public void onClick(View v) {
        if (next.getText().equals("Kontinua")) {
            String phone = userPhone.getText().toString();
            if (!TextUtils.isEmpty(phone)) {
                ccp.registerCarrierNumberEditText(userPhone);
                final String no_hp = ccp.getFullNumberWithPlus();
//                sendVerificationCode(no_hp);
                sendVerifikasiAuth(no_hp);
            } else {
                loginLayout.startAnimation(shakeAnimation);
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_no_hp, Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                userPhone.requestFocus();
            }
        } else if (next.getText().equals("Verify")) {
            String otpcode = pinView.getText().toString();
            ccp.registerCarrierNumberEditText(userPhone);
            final String no_hp = ccp.getFullNumber();
            if(!otpcode.isEmpty()){
//                verifikasiOtp(no_hp,otpcode);
                verifikasiOtpAuth(otpcode);
            }else{
                otpLayout.startAnimation(shakeAnimation);
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_otp, Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                ccp.requestFocus();
            }
        } else if (next.getText().equals("OK")) {
            ccp.registerCarrierNumberEditText(userPhone);
            final String no_hp = ccp.getFullNumber();
            final String namana = nama.getText().toString();
            final String jns_kelna = jns_kel.getSelectedItem().toString();
            if(namana.isEmpty()){
                profileLayout.startAnimation(shakeAnimation);
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_nama, Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                nama.requestFocus();
            }else if(jns_kelna.isEmpty()){
                profileLayout.startAnimation(shakeAnimation);
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_nama, Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                jns_kel.requestFocus();
            }else{
                simpanmember(no_hp,namana,jns_kelna);
            }
        }
    }
    private void verifikasiOtpAuth(final String verificationCode){
        progressDialog.setTitle("Verification Otp..");
        progressDialog.setMessage("Favor hein uituan...");
        progressDialog.show();
        new Handler().postDelayed(() -> verifikasiAtuh(verificationCode), 500);

    }
    private void verifikasiAtuh(final String verificationCode){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
        signInWithPhoneAuthCredential(credential);
    }
    private void sendVerifikasiAuth(final String phoneNumber){
        next.setText("Verify");
        first.setVisibility(View.GONE);
        second.setVisibility(View.VISIBLE);
        topText.setText("Hau seidauk fiar ita boot. Favor hatama kodigu OTP ne'ebe haruka ona liu SMS.");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                ActivityLoginHp.this,
                mCallbacks);
    }

    private void simpanmember(final String no_hp,final String namana,final String jns_kelna) {
        progressDialog.setTitle("Verification Data..");
        progressDialog.setMessage("Favor hein uituan...");
        progressDialog.show();
        new Handler().postDelayed(() -> simpandata(no_hp,namana,jns_kelna), 500);
    }
    private void simpandata(final String no_hpna,final String namana,final String jns_kelna) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_regis";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_SIMPAN_MEMBER, response -> {
                        hideDialog();
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {

                                SharedPreferences.Editor no_hp = userPref.edit();
                                no_hp.putString("no_hp",no_hpna);
                                no_hp.commit();

                                SharedPreferences.Editor nama = userPref.edit();
                                nama.putString("nama",namana);
                                nama.commit();

                                SharedPreferences.Editor jns_kel = userPref.edit();
                                jns_kel.putString("jns_kel",jns_kelna);
                                jns_kel.commit();

                                SharedPreferences.Editor foto = userPref.edit();
                                if(jns_kelna.equals("Male")){
                                    foto.putString("foto","male.png");
                                }else{
                                    foto.putString("foto","female.jpg");
                                }
                                foto.commit();

                                SharedPreferences.Editor isLogin = userPref.edit();
                                isLogin.putBoolean("is_login",true);
                                isLogin.commit();

                                Intent intent = new Intent(ActivityLoginHp.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
//                                finish();

                            } else {
                                dialogGagal();
                            }
                        } catch (JSONException e) {
                            Log.e("Data","Nganu : " + e);
                        }
                    }, error -> {
                hideDialog();
                dialogNoInternet();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("no_hp",no_hpna);
                    params.put("nama",namana);
                    params.put("jns_kel",jns_kelna);
                    Log.e("Data","Hasil Parsing : " + params);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    private void verifikasiOtp(final String no_hp,final String otpcode) {
        progressDialog.setTitle("Verification Otp..");
        progressDialog.setMessage("Favor hein uituan...");
        progressDialog.show();
        new Handler().postDelayed(() -> verifikasi(no_hp,otpcode), 500);
    }
    private void sendVerificationCode(final String no_hp) {
        progressDialog.setTitle("Sending Otp..");
        progressDialog.setMessage("Please Wait while Sending OTP..");
        progressDialog.show();
        new Handler().postDelayed(() -> submitReg(no_hp), 500);
    }
    private void verifikasi(final String no_hp,final String otp) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_regis";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_VERIF, response -> {
                        hideDialog();
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                pinView.setLineColor(Color.GREEN);
                                textU.setText("OTP Verified");
                                textU.setTextColor(Color.GREEN);
                                next.setText("Kontinua");
                                new Handler().postDelayed(() -> {

                                    next.setText("OK");
                                    first.setVisibility(View.GONE);
                                    second.setVisibility(View.GONE);
                                    tiga.setVisibility(View.VISIBLE);
                                    topText.setText("Kompletu o nia informasaun !");

                                }, 500);
                            } else {
                                pinView.setLineColor(Color.RED);
                                textU.setText("X Incorrect OTP");
                                textU.setTextColor(Color.RED);
                            }
                        } catch (JSONException e) {
                            Log.e("Data","Nganu : " + e);
                        }
                    }, error -> {
                        hideDialog();
                        dialogNoInternet();
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("no_hp",no_hp);
                    params.put("otp",otp);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    private void submitReg(final String no_hp) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_regis";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_REGIS, response -> {
                        hideDialog();
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {

                                next.setText("Verify");
                                first.setVisibility(View.GONE);
                                second.setVisibility(View.VISIBLE);
                                topText.setText("Hau seidauk fiar ita boot. Favor hatama kodigu OTP ne'ebe haruka ona liu SMS.");

                            } else {
                                dialogGagal();
                            }
                        } catch (JSONException e) {
                            Log.e("Data","Nganu : " + e);
                        }
                    }, error -> {
                        hideDialog();
                        dialogNoInternet();
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("no_hp",no_hp);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_login, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
    public void onBackPressed() {
        //do nothing
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

}
