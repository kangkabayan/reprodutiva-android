package coder.reprodutiva;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.getbase.floatingactionbutton.FloatingActionButton;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.MemoryCache;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.Tools;
import coder.reprodutiva.widget.AnimationsUtil;

public class ProfileActivity extends AppCompatActivity {
    FloatingActionButton shareBtn;
    ImageView sendMessageBtn,callVoice;
    String no_hp,nama,foto_profile;
    Toolbar toolbar;
    ImageView UserCover;
    TextView numberPhone,nama_text,jns_kel,tgl_join;
    MemoryCache memoryCache;
    ProgressDialog progressDialog = null;
    private static final int REQUEST_CALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detil);
        initcomponent();
        memoryCache = new MemoryCache();
    }
    @SuppressLint("StaticFieldLeak")
    private void initcomponent(){
        progressDialog = new ProgressDialog(ProfileActivity.this);
        Bundle b = getIntent().getExtras();
        assert b != null;
        no_hp = b.getString("no_hp");
        foto_profile = b.getString("foto_profile");
        nama = b.getString("nama");
        cekdata(no_hp);
        //shareBtn = findViewById(R.id.shareBtn);
        //sendMessageBtn = findViewById(R.id.send_message);
        //callVoice = findViewById(R.id.call_voice);
        //callVoice.setOnClickListener(view -> call());
        //shareBtn.setOnClickListener(view -> shareContact(no_hp,nama));
        //sendMessageBtn.setOnClickListener(view -> sendMessage(no_hp,nama));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        UserCover = findViewById(R.id.cover);
        UserCover.setOnClickListener(v -> {
            ArrayList<String> images_list = new ArrayList<>();
            images_list.add(Constant.getURLimgMember(foto_profile));
            Intent i = new Intent(ProfileActivity.this, ActivityFullScreenFoto.class);
            i.putStringArrayListExtra(ActivityFullScreenFoto.EXTRA_IMGS, images_list);
            startActivity(i);
        });
    }
    private void call(){
        if (no_hp.trim().length() > 0) {
            if (ContextCompat.checkSelfPermission(ProfileActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ProfileActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + "+" + no_hp;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        }
    }
    private void cekdata(final String no_hp) {
        progressDialog.setMessage("Please wait...");
        showDialog();
        new Handler().postDelayed(() -> cariMember(no_hp), 500);
    }
    private void cariMember(final String no_hp) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_login";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_CEK_MEMBER, response -> {
                        hideDialog();
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                JSONObject objUser = jObj.getJSONObject("data");

                                Tools.displayImageOriginal(ProfileActivity.this, UserCover, Constant.getURLimgMember(objUser.getString("foto")));
                                //numberPhone = findViewById(R.id.numberPhone);
                                //numberPhone.setText(no_hp);
                                nama_text = findViewById(R.id.nama);
                                nama_text.setText(objUser.getString("nama"));
                                jns_kel = findViewById(R.id.jns_kel);
                                if(objUser.getString("jns_kel").equals("1")){
                                    jns_kel.setText("Male");
                                }else{
                                    jns_kel.setText("Female");
                                }
                                tgl_join = findViewById(R.id.tgl_join);
                                tgl_join.setText(objUser.getString("tgl_registrasi"));

                            } else {
                                dialogGagal();
                            }
                        } catch (JSONException e) {
                            Log.e("Data","Nganu : " + e);
                        }
                    }, error -> {
                        hideDialog();
                        dialogNoInternet();
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("no_hp",no_hp);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_profile, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
    private void shareContact(final String no_hp,final String nama) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/*");
        String subject = null;
        if (nama != null) {
            subject = nama;
        }
        if (no_hp != null) {
            if (subject != null) {
                subject = subject + " " + no_hp;
            } else {
                subject = no_hp;
            }
        }
        if (subject != null) {
            shareIntent.putExtra(Intent.EXTRA_TEXT, subject);
        }

        startActivity(Intent.createChooser(shareIntent, getString(R.string.shareContact)));
    }
    private void sendMessage(final String no_hp,final String nama) {
        Intent messagingIntent = new Intent(this, ActivityMulaiChat.class);
        messagingIntent.putExtra("no_hp_admin", no_hp);
        messagingIntent.putExtra("nama_admin", nama);
        startActivity(messagingIntent);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
        onBackAction();
    }
    private void onBackAction() {
        if (MainActivity.active) {
            finish();
        } else {
            Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                call();
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
