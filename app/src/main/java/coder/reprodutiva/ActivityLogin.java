package coder.reprodutiva;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.CommonClass;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.JSONReader;
import coder.reprodutiva.utils.NetworkCheck;

public class ActivityLogin extends AppCompatActivity {

    Toolbar toolbar;
    EditText username,password;
    CheckBox checkBox;
    Button login;
    private View parent_view;
    ProgressDialog progressDialog = null;
    JSONReader j_reader;
    CommonClass common;
    private static CheckBox show_hide_password;
    SharedPreferences userPref;
    private static Animation shakeAnimation;
    private static LinearLayout loginLayout;
    private static View view;
    private TextView login_hp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_hp = findViewById(R.id.login_hp);
        login_hp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityLogin.this, ActivityLoginHp.class);
                startActivity(intent);
            }
        });
        j_reader = new JSONReader(this);
        common = new CommonClass(this);
        userPref = PreferenceManager.getDefaultSharedPreferences(ActivityLogin.this);
        show_hide_password = findViewById(R.id.show_hide_password);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.loginBtn);
        progressDialog = new ProgressDialog(ActivityLogin.this);
        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        shakeAnimation = AnimationUtils.loadAnimation(this,
                R.anim.shake);

        loginLayout = findViewById(R.id.login_layout);
        @SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
        try {
            ColorStateList csl = ColorStateList.createFromXml(getResources(),
                    xrp);

            show_hide_password.setTextColor(csl);

        } catch (Exception e) {
        }
        show_hide_password
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton button,
                                                 boolean isChecked) {
                        if (isChecked) {
                            show_hide_password.setText(R.string.hint_hide_pwd);// change
                            password.setInputType(InputType.TYPE_CLASS_TEXT);
                            password.setTransformationMethod(HideReturnsTransformationMethod
                                    .getInstance());// show password
                        } else {
                            show_hide_password.setText(R.string.hint_show_pass);// change
                            password.setInputType(InputType.TYPE_CLASS_TEXT
                                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            password.setTransformationMethod(PasswordTransformationMethod
                                    .getInstance());// hide password

                        }

                    }
                });
    }
    private void submitForm() {
        String str_user = username.getText().toString().trim();
        String str_pass = password.getText().toString().trim();
        if (str_user.isEmpty()) {
            loginLayout.startAnimation(shakeAnimation);
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_username, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            username.requestFocus();
        }else if (str_pass.isEmpty()) {
            loginLayout.startAnimation(shakeAnimation);
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_password, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            password.requestFocus();
        }else if (str_pass.length()<6){
            loginLayout.startAnimation(shakeAnimation);
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "password minimal 6 karakter", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            password.requestFocus();
        }else if (str_user.length()<6){
            loginLayout.startAnimation(shakeAnimation);
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Username minimal 6 karakter", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            username.requestFocus();
        }else{
            hideKeyboard();
            do_login(str_user,str_pass);
        }
    }
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    private void do_login(final  String str_user,final  String str_pass) {
        progressDialog.setMessage("Please wait...");
        showDialog();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitLogin(str_user,str_pass);
            }
        }, 500);
    }
    private void submitLogin(final  String str_user,final  String str_pass) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_login";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideDialog();
                    try {
                        JSONObject jObj = new JSONObject(response);
                        boolean error = jObj.getBoolean("error");
                        if (!error) {
                            JSONObject objUser = jObj.getJSONObject("data");
                            common.setSession(Constant.COMMON_KEY,objUser.getString("no_hp"));
                            common.setSession(Constant.COMMON_NAMA,objUser.getString("nama"));

                            SharedPreferences.Editor no_hp = userPref.edit();
                            no_hp.putString("no_hp",objUser.getString("no_hp"));
                            no_hp.commit();

                            SharedPreferences.Editor foto = userPref.edit();
                            foto.putString("foto",objUser.getString("foto"));
                            foto.commit();

                            SharedPreferences.Editor nama = userPref.edit();
                            nama.putString("nama",objUser.getString("nama"));
                            nama.commit();

                            SharedPreferences.Editor isLogin = userPref.edit();
                            isLogin.putBoolean("is_login",true);
                            isLogin.commit();

                            Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            dialogGagal();
                        }
                    } catch (JSONException e) {
                        Log.e("Data","Nganu : " + e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideDialog();
                    dialogNoInternet();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username",str_user);
                    params.put("password",str_pass);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_login, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
    public void onBackPressed() {
        //do nothing
    }
}