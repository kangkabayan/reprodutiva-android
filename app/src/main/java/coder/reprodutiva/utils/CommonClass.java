package coder.reprodutiva.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import coder.reprodutiva.data.Constant;


public class CommonClass {
    Activity activity;
    public SharedPreferences settings;

    public CommonClass(Activity activity){
        this.activity = activity;
        settings = activity.getSharedPreferences(Constant.PREF_NAME, 0);
    }
    public void setSession(String key, String value){
        settings.edit().putString(key,value).commit();
    }
    public String getSession(String key){
        return settings.getString(key,"");
    }
    public boolean is_user_login(){
        String key = getSession(Constant.COMMON_KEY);
        if (key==null || key.equalsIgnoreCase("")){
            return  false;
        }else {
            return  true;
        }
    }

}
