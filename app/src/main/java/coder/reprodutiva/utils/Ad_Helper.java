package coder.reprodutiva.utils;


import java.util.Arrays;
import java.util.Random;

public class Ad_Helper {
    private static int bigAdCounter = 0;
    public static int adCount = 1;
    public static int adCountGlobal = 1;

    public static final String AD_BANNER = "BANNER";
    public static final String AD_BANNER_MEDIUM = "MEDUIM_RECTANGLE";
    public static final String AD_NATIVE_SMALL = "NSMALL";
    public static final String AD_NATIVE_MEDIUM = "NMEDIUM";
    public static final String AD_NATIVE_BIG = "NBIG";


    public static String[] array = {Ad_Helper.AD_BANNER,Ad_Helper.AD_BANNER_MEDIUM,Ad_Helper.AD_NATIVE_BIG,
            Ad_Helper.AD_NATIVE_SMALL,Ad_Helper.AD_NATIVE_MEDIUM};
    public static String randomStr = array[new Random().nextInt(array.length)];

    public static boolean wannaSendPrivate(int n) {
        int[] bob = {5,15,20,30,35,45,50,60,65,70,80,100,120,150,170,200};
        Arrays.sort(bob);
        return Arrays.binarySearch(bob, n) >= 0;
    }

    public static boolean wannaSendGlobal(int n) {
        int[] bob = {5,15,20,30,35,45,50,60,65,70,80,100,120,150,170,200};
        Arrays.sort(bob);
        return Arrays.binarySearch(bob, n) >= 0;
    }


}