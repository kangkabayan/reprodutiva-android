package coder.reprodutiva.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import coder.reprodutiva.ActivityBerita;
import coder.reprodutiva.ActivityDialogNotification;
import coder.reprodutiva.R;


public class FcmMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        sendNotification(remoteMessage);

    }
    private void sendNotification(RemoteMessage remoteMessage) {
        if(remoteMessage.getNotification().getTitle().equals("Informasi Berita")){
            Intent intent = new Intent(this, ActivityBerita.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(100) , intent,
                    PendingIntent.FLAG_ONE_SHOT);
            long when = System.currentTimeMillis();
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this);
            mNotifyBuilder.setVibrate(new long[] { 1000, 1000,1000,1000,1000,1000});
            boolean jellybean = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
            if (jellybean) {
                mNotifyBuilder = new NotificationCompat.Builder(this)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setStyle(
                                new NotificationCompat.BigTextStyle()
                                        .bigText(remoteMessage.getNotification().getBody()))
                        .setContentText(remoteMessage.getNotification().getBody())
//                        .setColor(Color.TRANSPARENT)
                        .setLargeIcon(
                                BitmapFactory.decodeResource(
                                        getResources(),
                                        R.drawable.main_logo))
                        .setSmallIcon(R.drawable.ic_lonceng)
                        .setWhen(when).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

            } else {
                mNotifyBuilder = new NotificationCompat.Builder(this)
                        .setStyle(
                                new NotificationCompat.BigTextStyle()
                                        .bigText(remoteMessage.getNotification().getBody()))
                        .setContentTitle(remoteMessage.getNotification().getTitle()).setContentText(remoteMessage.getNotification().getBody())
                        .setSmallIcon(R.drawable.ic_lonceng)

                        .setWhen(when).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);
            }
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(new Random().nextInt(100) /* ID of notification */, mNotifyBuilder.build());
        }else{
            Intent intent = new Intent(this, ActivityDialogNotification.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(100) , intent,
                    PendingIntent.FLAG_ONE_SHOT);
            long when = System.currentTimeMillis();
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this);
            mNotifyBuilder.setVibrate(new long[] { 1000, 1000,1000,1000,1000,1000});
            boolean jellybean = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
            if (jellybean) {
                mNotifyBuilder = new NotificationCompat.Builder(this)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setStyle(
                                new NotificationCompat.BigTextStyle()
                                        .bigText(remoteMessage.getNotification().getBody()))
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setLargeIcon(
                                BitmapFactory.decodeResource(
                                        getResources(),
                                        R.drawable.main_logo))
                        .setSmallIcon(R.drawable.ic_lonceng)

                        .setWhen(when).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

            } else {
                mNotifyBuilder = new NotificationCompat.Builder(this)
                        .setStyle(
                                new NotificationCompat.BigTextStyle()
                                        .bigText(remoteMessage.getNotification().getBody()))
                        .setContentTitle(remoteMessage.getNotification().getTitle()).setContentText(remoteMessage.getNotification().getBody())
                        .setSmallIcon(R.drawable.ic_lonceng)
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(BitmapFactory.decodeResource(
                                        getResources(),
                                        R.drawable.ic_lonceng))
                                .bigLargeIcon(null))
                        .setWhen(when).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(new Random().nextInt(100) /* ID of notification */, mNotifyBuilder.build());
        }
    }
}