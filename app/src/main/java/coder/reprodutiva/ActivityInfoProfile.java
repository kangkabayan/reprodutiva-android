package coder.reprodutiva;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


import java.util.ArrayList;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.Tools;
import coder.reprodutiva.widget.AnimationsUtil;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class ActivityInfoProfile extends AppCompatActivity {
    EmojiconTextView userProfileName;
    String no_hp,foto_profile,nama;
    AppCompatImageView AboutBtn;
    private Intent mIntent;
    LinearLayout containerProfile,containerProfileInfo;
    private long Duration = 500;
    AppCompatImageView ContactBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Tools.isAndroid5()) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setStatusBarColor(Tools.getColor(this, R.color.colorPrimaryDark));
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.activity_info_profile);
        setupProgressBar();
        initcomponent();
        userProfileName.setSelected(true);
        if (Tools.isAndroid5()) {
            containerProfileInfo.post(() -> AnimationsUtil.show(containerProfileInfo, Duration));
        }
    }
    private void initcomponent(){
        Bundle b = getIntent().getExtras();
        assert b != null;
        no_hp = b.getString("no_hp");
        nama = b.getString("nama");
        foto_profile = b.getString("foto_profile");
        userProfileName = findViewById(R.id.userProfileName);
        if (Tools.isAndroid5()) {
            userProfileName.setTransitionName(getString(R.string.user_name_transition));
        }
        userProfileName.setText(nama);
        ImageView image =  findViewById(R.id.userProfilePicture);
        containerProfile = findViewById(R.id.containerProfile);
        containerProfileInfo = findViewById(R.id.containerProfileInfo);
        ContactBtn = findViewById(R.id.ContactBtn);
        ContactBtn.setOnClickListener(v -> {
            Intent messagingIntent = new Intent(this, ActivityMulaiChat.class);
            messagingIntent.putExtra("no_hp_admin", no_hp);
            messagingIntent.putExtra("nama_admin", nama);
            startActivity(messagingIntent);
            AnimationsUtil.setSlideInAnimation(this);
            finish();
        });
        AboutBtn = findViewById(R.id.AboutBtn);
        AboutBtn.setOnClickListener(v -> {
            mIntent = new Intent(this, ProfileActivity.class);
            mIntent.putExtra("no_hp", no_hp);
            mIntent.putExtra("nama", nama);
            mIntent.putExtra("foto_profile", foto_profile);
            startActivity(mIntent);
            AnimationsUtil.setSlideInAnimation(this);
            finish();
        });
        Tools.displayImageOriginal(ActivityInfoProfile.this, image, Constant.getURLimgMember(foto_profile));
        containerProfile.setOnClickListener(v -> {
            if (Tools.isAndroid5())
                containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
            else
                finish();
        });
        containerProfileInfo.setOnClickListener(v -> {
            if (Tools.isAndroid5())
                containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
            else
                finish();
        });
        image.setOnClickListener(v -> {
            ArrayList<String> images_list = new ArrayList<>();
            images_list.add(Constant.getURLimgMember(foto_profile));
            Intent i = new Intent(ActivityInfoProfile.this, ActivityFullScreenFoto.class);
            i.putStringArrayListExtra(ActivityFullScreenFoto.EXTRA_IMGS, images_list);
            startActivity(i);
        });
    }
    private void setupProgressBar() {
        ProgressBar mProgress = findViewById(R.id.progress_bar);
        mProgress.getIndeterminateDrawable().setColorFilter(Color.parseColor("#0EC654"),
                PorterDuff.Mode.SRC_IN);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        if (Tools.isAndroid5())
            containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
        else
            finish();
    }
}
