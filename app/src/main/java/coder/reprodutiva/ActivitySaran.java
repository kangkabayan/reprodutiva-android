package coder.reprodutiva;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.NetworkCheck;

public class ActivitySaran extends AppCompatActivity {
    private View parent_view;
    private Toolbar toolbar;
    private ActionBar actionBar;
    SharedPreferences userPref;
    MaterialRippleLayout lyt_saran;
    private EditText saran;
    ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saran);
        parent_view = findViewById(android.R.id.content);
        iniComponent();
        setupToolbar();
    }
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Informasi Saran");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    public void iniComponent() {
        progressDialog = new ProgressDialog(ActivitySaran.this);
        userPref = PreferenceManager.getDefaultSharedPreferences(ActivitySaran.this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        saran = findViewById(R.id.saran);
        lyt_saran = findViewById(R.id.lyt_saran);
        lyt_saran.setOnClickListener(view -> submitForm());
    }
    private void submitForm() {
        String isi_saran = saran.getText().toString().trim();
        if (isi_saran.isEmpty()) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  R.string.invalid_saran, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }else{
            hideKeyboard();
            do_kirim(isi_saran);
        }
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
    private void do_kirim(final  String isi_saran) {
        progressDialog.setMessage("Please wait...");
        showDialog();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitSaran(isi_saran);
            }
        }, 2000);
    }
    private void submitSaran(final  String isi_saran) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            final String no_hp = userPref.getString("no_hp", "");
            String tag_string_req = "req_kirim_saran";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_SARAN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideDialog();
                    try {
                        JSONObject jObj = new JSONObject(response);
                        boolean error = jObj.getBoolean("error");
                        if (!error) {
                            dialogBerhasil();
                        } else {
                            dialogGagal();
                        }
                    } catch (JSONException e) {
                        Log.e("Data","NGanu : " + e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideDialog();
                    dialogNoInternet();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("isi_saran",isi_saran);
                    params.put("no_hp",no_hp);
                    Log.e("Data","Parsing : " + params);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogBerhasil() {
        Dialog dialog = new DialogUtils(this).buildDialogInfo(R.string.title_info, R.string.msg_berhasil_saran, R.string.OK, R.drawable.img_checkout_success, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                saran.setText("");
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_saran, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        super.onBackPressed();
    }
}
