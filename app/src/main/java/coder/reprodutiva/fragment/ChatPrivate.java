package coder.reprodutiva.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.ActivityChat;
import coder.reprodutiva.ActivityMulaiChat;
import coder.reprodutiva.R;
import coder.reprodutiva.adapter.AdapterDataPrivate;
import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackAdmin;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.DataAdmin;
import coder.reprodutiva.utils.LoaderView;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.widget.AnimationsUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatPrivate extends Fragment {
    private View root_view;
    SharedPreferences userPref;
    private AdapterDataPrivate adapter;
    private Call<CallbackAdmin> callbackCall;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe_refresh;
    ProgressDialog progressDialog = null;
    private int failed_page = 0;
    LoaderView loader;
    private String query = "";
    private int post_total = 0;


    public static ChatPrivate newInstance() {
        return new ChatPrivate();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_private, null);
        initComponent();
        swipeProgress(true);
        return root_view;
    }
    private void requestDataADmin(final int page_no) {
        API api = RestAdapter.createAPI();
        final String no_hp = userPref.getString("no_hp", "");
        callbackCall = api.getDataPrivate(page_no, Constant.REQ_BERITA, query, no_hp);
        callbackCall.enqueue(new Callback<CallbackAdmin>() {
            @Override
            public void onResponse(Call<CallbackAdmin> call, Response<CallbackAdmin> response) {
                CallbackAdmin resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    displayApiResult(resp.admins);
                } else {
                    onFailRequest();
                }
            }
            @Override
            public void onFailure(Call<CallbackAdmin> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest();
            }
        });
    }
    private void displayApiResult(final List<DataAdmin> items) {
        adapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) {
            showNoItemView(true);
        }
    }
    private void initComponent() {
        swipe_refresh = root_view.findViewById(R.id.swipe_refresh_layout);
        userPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        recyclerView = root_view.findViewById(R.id.recyclerViewPrivate);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        loader = new LoaderView(getActivity());

        adapter = new AdapterDataPrivate(getActivity(), recyclerView, new ArrayList<>());

        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);
        swipe_refresh.setOnRefreshListener(() -> {
            if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
            adapter.resetListData();
            requestAction(1);
        });
        adapter.setOnItemClickListener((view, obj) -> {
            Bundle b = new Bundle();
            Intent i;
            i = new Intent(ChatPrivate.this.getActivity(), ActivityMulaiChat.class);
            b.putString("no_hp_admin", obj.no_hp);
            b.putString("nama_admin", obj.nama);
            i.putExtras(b);
            AnimationsUtil.setSlideInAnimation(getActivity());
            ChatPrivate.this.startActivity(i);
        });
        adapter.setOnLoadMoreListener(current_page -> {
            if (post_total > adapter.getItemCount() && current_page != 0) {
                int next_page = current_page + 1;
                requestAction(next_page);
            } else {
                adapter.setLoaded();
            }
        });
        progressDialog = new ProgressDialog(getActivity());
        showNoItemView(false);
        requestAction(1);
    }
    @Override
    public void onResume() {
        adapter.notifyDataSetChanged();
        super.onResume();
    }
    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            adapter.setLoading();
        }
        new Handler().postDelayed(() -> requestDataADmin(page_no), 500);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }
    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(() -> swipe_refresh.setRefreshing(show));
    }
    private void showFailedView(boolean show, String message) {
        View lyt_failed = root_view.findViewById(R.id.lyt_failed);
        ((TextView) root_view.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        root_view.findViewById(R.id.failed_retry).setOnClickListener(view -> requestAction(failed_page));
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = root_view.findViewById(R.id.lyt_no_item);
        ((TextView) root_view.findViewById(R.id.no_item_message)).setText("Data Admin Not Found");
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat, menu);
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityChat.getInstance().showDialogFailed(message);
    }
}
