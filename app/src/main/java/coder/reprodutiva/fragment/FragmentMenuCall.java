package coder.reprodutiva.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import coder.reprodutiva.ActivityChat;
import coder.reprodutiva.ActivityFaq;
import coder.reprodutiva.ActivityPeriode;
import coder.reprodutiva.MainActivity;
import coder.reprodutiva.R;
import coder.reprodutiva.adapter.AdapterMenuCall;
import coder.reprodutiva.adapter.AdapterMenuMaster;
import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackMenu;
import coder.reprodutiva.connection.callbacks.CallbackMenuCall;
import coder.reprodutiva.utils.NetworkCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMenuCall extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private Call<CallbackMenuCall> callbackCall;
    private AdapterMenuCall adapter;
    SharedPreferences userPref;
    private static final int REQUEST_CALL = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_menu_call, null);
        initComponent();
        requestMenu();
        return root_view;
    }

    private void initComponent() {
        userPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        recyclerView = root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        adapter = new AdapterMenuCall(getActivity(), recyclerView, new ArrayList<>());

        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener((view, obj) -> {
            String number = obj.no_tlp;
            if (number.trim().length() > 0) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
                } else {
                    String dial = "tel:" + number;
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                }
            }
        });
    }
    private void requestMenu() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getListMenuCall();
        callbackCall.enqueue(new Callback<CallbackMenuCall>() {
            @Override
            public void onResponse(Call<CallbackMenuCall> call, Response<CallbackMenuCall> response) {
                CallbackMenuCall resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.menus);
                    MainActivity.getInstance().menu_call = true;
                    MainActivity.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }
            }
            @Override
            public void onFailure(Call<CallbackMenuCall> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }
        });
    }
    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        MainActivity.getInstance().showDialogFailed(message);
    }
}
