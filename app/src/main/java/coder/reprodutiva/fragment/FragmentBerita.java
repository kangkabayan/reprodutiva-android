package coder.reprodutiva.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.ActivityBeritaDetil;
import coder.reprodutiva.MainActivity;
import coder.reprodutiva.R;
import coder.reprodutiva.adapter.AdapterBerita;
import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackBerita;
import coder.reprodutiva.model.NewsInfo;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentBerita extends Fragment {

    private View root_view;
    private ViewPager viewPager;
    private Handler handler = new Handler();
    private Runnable runnableCode = null;
    private AdapterBerita adapter;
    private Call<CallbackBerita> callbackCall;
    private TextView features_news_title;
    private View lyt_main_content;
    private ImageButton bt_previous, bt_next;
    private LinearLayout layout_dots;
    SharedPreferences userPref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_berita, null);
        initComponent();
        requestFeaturedNews();
        return root_view;
    }

    private void initComponent() {
        userPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        lyt_main_content = root_view.findViewById(R.id.lyt_cart);
        features_news_title = root_view.findViewById(R.id.featured_news_title);
        layout_dots = root_view.findViewById(R.id.layout_dots);
        viewPager = root_view.findViewById(R.id.pager);
        bt_previous = root_view.findViewById(R.id.bt_previous);
        bt_next = root_view.findViewById(R.id.bt_next);
        adapter = new AdapterBerita(getActivity(), new ArrayList<>());
        lyt_main_content.setVisibility(View.GONE);

        bt_previous.setOnClickListener(view -> prevAction());

        bt_next.setOnClickListener(view -> nextAction());
    }

    private void displayResultData(List<NewsInfo> items) {
        adapter.setItems(items);
        viewPager.setAdapter(adapter);

        LayoutParams params = viewPager.getLayoutParams();
        params.height = Tools.getFeaturedNewsImageHeight(getActivity());
        viewPager.setLayoutParams(params);

        // displaying selected image first
        viewPager.setCurrentItem(0);
        features_news_title.setText(adapter.getItem(0).judul);
        addBottomDots(layout_dots, adapter.getCount(), 0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                NewsInfo cur = adapter.getItem(pos);
                features_news_title.setText(cur.judul);
                addBottomDots(layout_dots, adapter.getCount(), pos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        startAutoSlider(adapter.getCount());

        adapter.setOnItemClickListener((view, obj) -> {
            if(obj.id==0){
                Snackbar.make(root_view, obj.judul, Snackbar.LENGTH_SHORT).show();
            }else{
                ActivityBeritaDetil.navigate(getActivity(), obj.id, false);
            }
        });

        lyt_main_content.setVisibility(View.VISIBLE);
        MainActivity.getInstance().news_load = true;
        MainActivity.getInstance().showDataLoaded();
    }

    private void requestFeaturedNews() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getBerita();
        callbackCall.enqueue(new Callback<CallbackBerita>() {
            @Override
            public void onResponse(Call<CallbackBerita> call, Response<CallbackBerita> response) {
                CallbackBerita resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    displayResultData(resp.news_infos);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackBerita> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void startAutoSlider(final int count) {
        runnableCode = () -> {
            int pos = viewPager.getCurrentItem();
            pos = pos + 1;
            if (pos >= count) pos = 0;
            viewPager.setCurrentItem(pos);
            handler.postDelayed(runnableCode, 3000);
        };
        handler.postDelayed(runnableCode, 3000);
    }

    private void prevAction() {
        int pos = viewPager.getCurrentItem();
        pos = pos - 1;
        if (pos < 0) pos = adapter.getCount();
        viewPager.setCurrentItem(pos);
    }

    private void nextAction() {
        int pos = viewPager.getCurrentItem();
        pos = pos + 1;
        if (pos >= adapter.getCount()) pos = 0;
        viewPager.setCurrentItem(pos);
    }

    @Override
    public void onDestroy() {
        if (runnableCode != null) handler.removeCallbacks(runnableCode);
        super.onDestroy();
    }

    private void addBottomDots(LinearLayout layout_dots, int size, int current) {
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(getActivity());
            int width_height = 10;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(getActivity(), R.color.darkOverlaySoft));
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current].setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimaryLight));
        }
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        MainActivity.getInstance().showDialogFailed(message);
    }
}
