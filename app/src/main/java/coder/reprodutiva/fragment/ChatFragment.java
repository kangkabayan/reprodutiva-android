package coder.reprodutiva.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import coder.reprodutiva.ActivityBerita;
import coder.reprodutiva.ActivityBeritaDetil;
import coder.reprodutiva.ActivityFaq;
import coder.reprodutiva.ActivityFaqDetil;
import coder.reprodutiva.AppController;
import coder.reprodutiva.ImagePickerActivity;
import coder.reprodutiva.MainActivity;
import coder.reprodutiva.R;
import coder.reprodutiva.adapter.chat_adapter;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.LoaderView;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.RecyclerTouchListener;
import coder.reprodutiva.widget.ReplyMessage;





public class ChatFragment extends Fragment {
    private View root_view;
    RecyclerView recyclerView;
    public static final int REQUEST_IMAGE = 100;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager manager;
    Socket socket;
    ArrayList<String> chat_data=new ArrayList<>();
    ArrayList<String> pengirim_=new ArrayList<>();
    ArrayList<String> foto_pengirim_=new ArrayList<>();
    ArrayList<String> jam_kirim_ =new ArrayList<>();
    ArrayList<String> saha_ =new ArrayList<>();
    ArrayList<String> image_ =new ArrayList<>();
    ProgressDialog progressDialog = null;
    LinearLayout inputReplyLayout;

    LoaderView loader;
    ImageButton kirim;
    private ImageButton smileyButton;
    private EmojiEditText chat;
    private EmojiPopup emojiPopup;
    private final int OPEN_IMAGE = 536;
    SharedPreferences userPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_chat, null);
        userPref = android.preference.PreferenceManager.getDefaultSharedPreferences(getActivity());
        initComponent();
        return root_view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    private void initComponent() {
        progressDialog = new ProgressDialog(getActivity());
        recyclerView = root_view.findViewById(R.id.list_chat_group);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter= new chat_adapter(getActivity(),chat_data,pengirim_,foto_pengirim_,jam_kirim_,saha_,image_);
        recyclerView.setAdapter(adapter);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        smileyButton = root_view.findViewById(R.id.button_smiley);
        chat = root_view.findViewById(R.id.input_text_chat);
        kirim = root_view.findViewById(R.id.button_send);
        kirim.setOnClickListener(view -> {
            if (!NetworkCheck.isConnect(getActivity())) {
                dialogNoInternet();
            } else {
                kirim_pesan();
                chat.setText("");
            }
        });

        emojiPopup = EmojiPopup.Builder.fromRootView(root_view)
                .setOnEmojiPopupShownListener(() -> smileyButton.setImageResource(R.drawable.ic_keyboard)).setOnEmojiPopupDismissListener(() -> smileyButton.setImageResource(R.drawable.ic_smiley)).setOnSoftKeyboardCloseListener(() -> emojiPopup.dismiss()).build(chat);

        smileyButton.setOnClickListener(view -> emojiPopup.toggle());
        root_view.findViewById(R.id.button_add_image).setOnClickListener(view -> {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            openImageBottomSheet();
        });
        try{
            socket= IO.socket(Constant.CHAT_SERVER_URL);
        }
        catch (URISyntaxException e){
            Log.d("error","onCreate"+e.toString());
        }
        socket.connect();
        socket.on("pesan",handling);
        adapter.notifyDataSetChanged();
        fetchChatThread();
    }
    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.off("pesan");
        socket.disconnect();
    }

    private void openImageBottomSheet() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, OPEN_IMAGE);
                return;
            }
        }
        showImagePickerOptions();
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(getActivity()).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                getActivity().finish();
            }
        });
        dialog.show();
    }
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            int length = charSequence.length();
            kirim.setVisibility(length > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    @Override
    public void onStart() {
        super.onStart();
        chat.addTextChangedListener(textWatcher);

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat, menu);
    }
    private Emitter.Listener handling = args -> getActivity().runOnUiThread(() -> tambahpesan(args[0].toString(),args[1].toString(),args[2].toString(),args[3].toString(),args[4].toString(),args[5].toString()));

    public void kirim_pesan(){
        final String nama = userPref.getString("nama", "");
        final String foto = userPref.getString("foto", "");
        final String no_hpna = userPref.getString("no_hp", "");
        String pesan = chat.getText().toString().trim();
        chat.setText("");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
        String jam_kirim = mdformat.format(calendar.getTime());
        String tag_string_req = "req_kirim_chat";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Constant.URL_CHAT_GROUP, response -> {
            try {
                JSONObject jObj = new JSONObject(response);
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    tambahpesan(pesan,nama,foto,jam_kirim,no_hpna,"no.jpg");
                    socket.emit("pesan",pesan,nama,foto,jam_kirim,no_hpna,"no.jpg");
                } else {
                    dialogGagal();
                }
            } catch (JSONException e) {
                Log.e("Data","Nganu : " + e);
            }
        }, error -> dialogNoInternet()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("isi_pesan",pesan);
                params.put("no_hp",no_hpna);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(getActivity()).buildDialogWarning(R.string.title_info, R.string.msg_gagal_kirim_pesan, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void tambahpesan(String pesan, String pengirim, String foto, String jam_kirim, String saha, String image) {
        chat_data.add(pesan);
        this.pengirim_.add(pengirim);
        this.foto_pengirim_.add(foto);
        this.jam_kirim_.add(jam_kirim);
        this.saha_.add(saha);
        this.image_.add(image);
        adapter.notifyItemInserted(chat_data.size()-1);
        recyclerView.smoothScrollToPosition(chat_data.size()-1);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void fetchChatThread() {
        if (!NetworkCheck.isConnect(getActivity())) {
            dialogNoInternet();
        } else {
            String tag_string_req = "req_login";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_GET_CHAT_GROUP, response -> {
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                JSONArray commentsObj = jObj.getJSONArray("data");

                                for (int i = 0; i < commentsObj.length(); i++) {
                                    JSONObject commentObj = (JSONObject) commentsObj.get(i);

                                    String no_hp = commentObj.getString("no_hp");
                                    String nama = commentObj.getString("nama");
                                    String tgl = commentObj.getString("tgl");
                                    String pesan = commentObj.getString("pesan");
                                    String foto = commentObj.getString("foto");
                                    String image = commentObj.getString("image");

                                    chat_data.add(pesan);
                                    pengirim_.add(nama);
                                    foto_pengirim_.add(foto);
                                    jam_kirim_.add(tgl);
                                    saha_.add(no_hp);
                                    image_.add(image);
                                }

                                adapter.notifyDataSetChanged();
                                if (adapter.getItemCount() > 1) {
                                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, adapter.getItemCount() - 1);
                                }

                            }
                        } catch (JSONException e) {
                            Log.e("Data","Nganu : " + e);
                        }
            }, error -> dialogNoInternet()) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    public void uploadServer(final String lokasi_foto) {
        if (!NetworkCheck.isConnect(getActivity())) {
            dialogNoInternet();
        } else {
            final String nama = userPref.getString("nama", "");
            final String foto = userPref.getString("foto", "");
            final String no_hpna = userPref.getString("no_hp", "");
            String pesan = chat.getText().toString().trim();
            chat.setText("");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
            String jam_kirim = mdformat.format(calendar.getTime());
            Random r = new Random();
            int randomNumber = r.nextInt(555555555);
            String tag_string_req = "req_ganti_upload_foto";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_UPLOAD_FOTO, response -> {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        tambahpesan("upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg");
                        socket.emit("pesan","upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg");
                    } else {
                        dialogGagal();
                    }
                } catch (JSONException e) {
                    Log.e("Data","NGanu : " + e);
                    dialogGagal();
                }
            }, error -> {
                dialogNoInternet();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("foto",lokasi_foto);
                    params.put("no_hp",no_hpna);
                    params.put("nama_file",String.valueOf(randomNumber));
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), uri);
                    new Handler().postDelayed(() -> getStringImage(bitmap), 2000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        uploadServer(encodedImage);
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(getActivity(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
}
