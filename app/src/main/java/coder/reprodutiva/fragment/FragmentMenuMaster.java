package coder.reprodutiva.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import coder.reprodutiva.ActivityChat;
import coder.reprodutiva.ActivityFaq;
import coder.reprodutiva.ActivityPeriode;
import coder.reprodutiva.MainActivity;
import coder.reprodutiva.R;
import coder.reprodutiva.adapter.AdapterMenuMaster;
import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackMenu;
import coder.reprodutiva.model.Menu;
import coder.reprodutiva.utils.NetworkCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMenuMaster extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private Call<CallbackMenu> callbackCall;
    private AdapterMenuMaster adapter;
    SharedPreferences userPref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_menu_master, null);
        initComponent();
        requestMenu();
        return root_view;
    }

    private void initComponent() {
        userPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        recyclerView = root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        adapter = new AdapterMenuMaster(getActivity(), recyclerView, new ArrayList<>());

        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener((view, obj) -> {
            if (obj.id == 1) {
                Intent intent = new Intent(getContext(), ActivityPeriode.class);
                startActivity(intent);
            } else if (obj.id == 2) {
                Intent intent = new Intent(getContext(), ActivityFaq.class);
                startActivity(intent);
            }else if (obj.id == 3) {
                Intent intent = new Intent(getContext(), ActivityChat.class);
                startActivity(intent);
            }
        });
    }
    private void requestMenu() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getListMenuMaster();
        callbackCall.enqueue(new Callback<CallbackMenu>() {
            @Override
            public void onResponse(Call<CallbackMenu> call, Response<CallbackMenu> response) {
                CallbackMenu resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.menus);
                    MainActivity.getInstance().menu_load = true;
                    MainActivity.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }
            }
            @Override
            public void onFailure(Call<CallbackMenu> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }
        });
    }
    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        MainActivity.getInstance().showDialogFailed(message);
    }
}
