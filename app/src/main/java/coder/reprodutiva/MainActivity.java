package coder.reprodutiva;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.getbase.floatingactionbutton.FloatingActionButton;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackDevice;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.data.SharedPref;
import coder.reprodutiva.fragment.FragmentBerita;
import coder.reprodutiva.fragment.FragmentMenuCall;
import coder.reprodutiva.fragment.FragmentMenuMaster;
import coder.reprodutiva.model.DeviceInfo;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private CardView search_bar;
    private SwipeRefreshLayout swipe_refresh;
    private View parent_view;
    private NavigationView nav_view;
    private SharedPref sharedPref;
    private Dialog dialog_failed = null;
    public boolean news_load = false,menu_load = false,menu_call = false;
    private static final int REQUEST_CALL = 1;
    SharedPreferences userPref;

    static MainActivity activityMain;
    Handler handler = new Handler();

    public static MainActivity getInstance() {
        return activityMain;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityMain = this;
        userPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        sharedPref = new SharedPref(this);
        sharedPref.setNoHp(userPref.getString("no_hp",""));
        obtainFirebaseToken();
        initToolbar();
        initDrawerMenu();
        initComponent();
        initFragment();
        swipeProgress(true);

        if (sharedPref.isFirstLaunch()) {
            startActivity(new Intent(this, ActivityInstruction.class));
            sharedPref.setFirstLaunch(false);
        }
    }
    Runnable runnable = () -> obtainFirebaseToken();
    private void obtainFirebaseToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        if (TextUtils.isEmpty(token)) {
            handler.removeCallbacks(runnable);
            handler.postDelayed(runnable, 10 * 1000);
        } else {
            sendRegistrationToServer(token);
        }
    }
    private void initToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar = findViewById(R.id.toolbar);
        }
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.app_name);
    }
    private void sendRegistrationToServer(String token) {
        if (NetworkCheck.isConnect(this) && !TextUtils.isEmpty(token) && sharedPref.isOpenAppCounterReach()) {
            final String no_hp = userPref.getString("no_hp", "");
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.device = Tools.getDeviceName();
            deviceInfo.os_version = Tools.getAndroidVersion();
            deviceInfo.app_version = Tools.getVersionCode(this) + " (" + Tools.getVersionNamePlain(this) + ")";
            deviceInfo.serial = Tools.getDeviceID(this);
            deviceInfo.regid = token;
            deviceInfo.no_hp = no_hp;

            API api = RestAdapter.createAPI();
            Call<CallbackDevice> callbackCall = api.registerDevice(deviceInfo);
            callbackCall.enqueue(new Callback<CallbackDevice>() {
                @Override
                public void onResponse(Call<CallbackDevice> call, Response<CallbackDevice> response) {
                    CallbackDevice resp = response.body();
                    if (resp != null && resp.status.equals("success")) {
                        sharedPref.setOpenAppCounter(0);
                    }
                }

                @Override
                public void onFailure(Call<CallbackDevice> call, Throwable t) {
                }
            });
        }
    }
    private void initDrawerMenu() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView kode = headerView.findViewById(R.id.no_hp);
        kode.setText(userPref.getString("no_hp", ""));
        sharedPref = new SharedPref(this);
        sharedPref.setNoHp(userPref.getString("no_hp", ""));

        TextView nama =  headerView.findViewById(R.id.nama);
        nama.setText(userPref.getString("nama", ""));
        ImageView image =  headerView.findViewById(R.id.profileImageView);
        Tools.displayImageOriginal(MainActivity.this, image, Constant.getURLimgMember(userPref.getString("foto", "")));
        nav_view = findViewById(R.id.nav_view);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(item -> {
            onItemSelected(item.getItemId());
            //drawer.closeDrawers();
            return true;
        });
        nav_view.setItemIconTintList(getResources().getColorStateList(R.color.darkOverlay));
    }

    private void initFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentBerita fragmentFeaturedNews = new FragmentBerita();
        fragmentTransaction.replace(R.id.frame_berita, fragmentFeaturedNews);

        FragmentMenuMaster fragmentMenuMaster = new FragmentMenuMaster();
        fragmentTransaction.replace(R.id.frame_menu_master, fragmentMenuMaster);

        FragmentMenuCall fragmentMenuCall = new FragmentMenuCall();
        fragmentTransaction.replace(R.id.frame_menu_call, fragmentMenuCall);

        fragmentTransaction.commit();
    }

    private void initComponent() {
//        final FloatingActionButton actionTlpLaki = findViewById(R.id.konsultasi_laki);
//        actionTlpLaki.setOnClickListener(view -> tlp_cs_laki());
//        final FloatingActionButton actionTlpCewe = findViewById(R.id.konsultasi_wanita);
//        actionTlpCewe.setOnClickListener(view -> tlp_cs_cewe());
        parent_view = findViewById(R.id.parent_view);
        search_bar = findViewById(R.id.search_bar);
        swipe_refresh = findViewById(R.id.swipe_refresh_layout);
        NestedScrollView nested_content = findViewById(R.id.nested_content);
        nested_content.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY < oldScrollY) { // up
                animateSearchBar(false);
            }
            if (scrollY > oldScrollY) { // down
                animateSearchBar(true);
            }
        });
        // on swipe list
        swipe_refresh.setOnRefreshListener(() -> refreshFragment());

        findViewById(R.id.action_search).setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, ActivityBerita.class);
            startActivity(intent);
        });
    }
    private void tlp_cs_laki() {
        String number = "+6282117711166";
        if (number.trim().length() > 0) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        }
    }
    private void tlp_cs_cewe() {
        String number = "+6282117711122";
        if (number.trim().length() > 0) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tlp_cs_laki();
                tlp_cs_cewe();
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void refreshFragment() {
        menu_load = false;
        menu_call = false;
        news_load = false;
        swipeProgress(true);
        new Handler().postDelayed(() -> initFragment(), 500);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(() -> swipe_refresh.setRefreshing(show));
    }

    boolean isSearchBarHide = false;

    private void animateSearchBar(final boolean hide) {
        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
        isSearchBarHide = hide;
        int moveY = hide ? -(2 * search_bar.getHeight()) : 0;
        search_bar.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    static boolean active = false;

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        active = false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.openDrawer(GravityCompat.START);
        } else {
            doExitApp();
        }
    }

    private long exitTime = 0;
    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

    public void showDataLoaded() {
        if (news_load && menu_load && menu_call) {
            swipeProgress(false);
        }else{
            swipeProgress(false);
        }
    }

    public void showDialogFailed(@StringRes int msg) {
        if (dialog_failed != null && dialog_failed.isShowing()) return;
        swipeProgress(false);
        dialog_failed = new DialogUtils(this).buildDialogWarning(-1, msg, R.string.TRY_AGAIN, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                refreshFragment();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog_failed.show();
    }
    public boolean onItemSelected(int id) {
        Intent i;
        switch (id) {
            case R.id.nav_home:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
            case R.id.nav_periode:
                i = new Intent(this, ActivityPeriode.class);
                startActivity(i);
                break;
            case R.id.nav_info:
                i = new Intent(this, ActivityFaq.class);
                startActivity(i);
                break;
            case R.id.nav_news:
                i = new Intent(this, ActivityBerita.class);
                startActivity(i);
                break;
            case R.id.nav_saran:
                i = new Intent(this, ActivitySaran.class);
                startActivity(i);
                break;
            case R.id.nav_chat:
                i = new Intent(this, ActivityChat.class);
                startActivity(i);
                break;
            case R.id.nav_profile:
                i = new Intent(this, ActivityProfile.class);
                startActivity(i);
                break;
            case R.id.nav_about:
                Tools.showDialogAbout(this);
                break;
            case R.id.nav_logout:
                Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_log_out, R.string.content_log_out, R.string.OK, R.string.CANCEL, R.drawable.img_no_connect, new CallbackDialog() {
                    @Override
                    public void onPositiveClick(Dialog dialog) {
                        /*SharedPreferences myPrefs = getSharedPreferences(Constant.PREF_NAME,
                                MODE_PRIVATE);

                        SharedPreferences.Editor editor = myPrefs.edit();
                        editor.clear();
                        editor.commit();

                        SharedPreferences.Editor isLogin = userPref.edit();
                        isLogin.putBoolean("is_login",false);
                        isLogin.commit();

                        Intent intent1 = new Intent(MainActivity.this, ActivityLoginHp.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent1);
                        dialog.dismiss();*/
                        finish();
                        System.exit(0);
                    }
                    @Override
                    public void onNegativeClick(Dialog dialog) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            default:
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawers();
        return true;
    }
}
