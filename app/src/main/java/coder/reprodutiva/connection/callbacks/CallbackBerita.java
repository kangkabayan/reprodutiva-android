package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.NewsInfo;

public class CallbackBerita implements Serializable {

    public String status = "";
    public List<NewsInfo> news_infos = new ArrayList<>();
}
