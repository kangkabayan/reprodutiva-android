package coder.reprodutiva.connection.callbacks;
import java.io.Serializable;

import coder.reprodutiva.model.Info;

public class CallbackInfo implements Serializable {
    public String status = "";
    public Info info = null;
}
