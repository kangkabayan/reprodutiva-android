package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.DataAdmin;

public class CallbackAdmin implements Serializable {

    public String status = "";
    public List<DataAdmin> admins = new ArrayList<>();

}
