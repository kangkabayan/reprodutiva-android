package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.Chat;

public class CallbackChat implements Serializable {

    public String status = "";
    public int count = -1;
    public int count_total = -1;
    public int pages = -1;
    public List<Chat> chats = new ArrayList<>();

}
