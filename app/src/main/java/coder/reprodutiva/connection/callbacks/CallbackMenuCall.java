package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.MenuCall;


public class CallbackMenuCall implements Serializable {

    public String status = "";
    public List<MenuCall> menus = new ArrayList<>();

}
