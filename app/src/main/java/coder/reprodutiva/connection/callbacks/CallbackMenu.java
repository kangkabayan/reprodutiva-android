package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.Menu;

public class CallbackMenu implements Serializable {

    public String status = "";
    public List<Menu> menus = new ArrayList<>();

}
