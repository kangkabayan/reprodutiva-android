package coder.reprodutiva.connection.callbacks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.model.DataGroupChat;

public class CallbackGroupChat implements Serializable {

    public String status = "";
    public List<DataGroupChat> chats = new ArrayList<>();

}
