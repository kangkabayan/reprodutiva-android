package coder.reprodutiva.connection;

import coder.reprodutiva.connection.callbacks.CallbackAdmin;
import coder.reprodutiva.connection.callbacks.CallbackBerita;
import coder.reprodutiva.connection.callbacks.CallbackBeritaDetil;
import coder.reprodutiva.connection.callbacks.CallbackChat;
import coder.reprodutiva.connection.callbacks.CallbackDataBerita;
import coder.reprodutiva.connection.callbacks.CallbackDevice;
import coder.reprodutiva.connection.callbacks.CallbackFaq;
import coder.reprodutiva.connection.callbacks.CallbackGroupChat;
import coder.reprodutiva.connection.callbacks.CallbackInfo;
import coder.reprodutiva.connection.callbacks.CallbackMenu;
import coder.reprodutiva.connection.callbacks.CallbackMenuCall;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.model.DeviceInfo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: Reprodutiva";
    String SECURITY = "Security: " + Constant.SECURITY_CODE;

    /* Recipe API transaction ------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("info")
    Call<CallbackInfo> getInfo(
            @Query("version") int version
    );
    /*Data Berita Slider*/
    @Headers({CACHE, AGENT})
    @GET("getDataBerita")
    Call<CallbackBerita> getBerita(
    );
    /*Data Menu*/
    @Headers({CACHE, AGENT})
    @GET("getListMenuMaster")
    Call<CallbackMenu> getListMenuMaster(
    );
    /*Data Menu Call*/
    @Headers({CACHE, AGENT})
    @GET("getListMenuCall")
    Call<CallbackMenuCall> getListMenuCall(
    );
    /*Detil Data Berita*/
    @Headers({CACHE, AGENT})
    @GET("getDetilBerita")
    Call<CallbackBeritaDetil> getDetilBerita(
            @Query("id") long id
    );
    /*List Data Berita*/
    @Headers({CACHE, AGENT})
    @GET("getListBerita")
    Call<CallbackDataBerita> getListBerita(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query
    );
    /*Informasi Data FAQ*/
    @Headers({CACHE, AGENT})
    @GET("getFaq")
    Call<CallbackFaq> getFaq(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query
    );
    /*Data Admin*/
    @Headers({CACHE, AGENT})
    @GET("getDataAdmin")
    Call<CallbackAdmin> getDataADmin(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("no_hp") String no_hp
    );
    /*Data Admin*/
    @Headers({CACHE, AGENT})
    @GET("getDataChat")
    Call<CallbackChat> getDataChat(
            @Query("no_hp_tujuan") String no_hp_tujuan,
            @Query("no_hp") String no_hp
    );
    @Headers({CACHE, AGENT})
    @GET("getDataGroupChat")
    Call<CallbackGroupChat> getDataGroupChat(

    );
    /*Data Admin*/
    @Headers({CACHE, AGENT})
    @GET("getDataPrivate")
    Call<CallbackAdmin> getDataPrivate(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("no_hp") String no_hp
    );
    /* Fcm API ----------------------------------------------------------- */
    @Headers({CACHE, AGENT, SECURITY})
    @POST("insertOneFcm")
    Call<CallbackDevice> registerDevice(
            @Body DeviceInfo deviceInfo
    );
}
