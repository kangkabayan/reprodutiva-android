package coder.reprodutiva;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import coder.reprodutiva.adapter.AdapterFaq;
import coder.reprodutiva.connection.API;
import coder.reprodutiva.connection.RestAdapter;
import coder.reprodutiva.connection.callbacks.CallbackFaq;
import coder.reprodutiva.model.Faq;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.LoaderView;
import coder.reprodutiva.utils.NetworkCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFaq extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterFaq mAdapter;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackFaq> callbackCall = null;
    private ImageButton bt_clear,bt_add;
    private EditText et_search;
    SharedPreferences userPref;
    ProgressDialog progressDialog = null;
    private View parent_view;
    private int post_total = 0;
    private int failed_page = 0;
    private String query = "";
    LoaderView loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        parent_view = findViewById(android.R.id.content);
        iniComponent();
        setupToolbar();
    }
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence c, int i, int i1, int i2) {
            if (c.toString().trim().length() == 0) {
                bt_clear.setVisibility(View.GONE);
            } else {
                bt_clear.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };
    @Override
    protected void onResume() {
        mAdapter.notifyDataSetChanged();
        super.onResume();
    }

    public void iniComponent() {
        swipe_refresh = findViewById(R.id.swipe_refresh_layout);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bt_clear = findViewById(R.id.bt_clear);
        bt_clear.setVisibility(View.GONE);
        et_search = findViewById(R.id.et_search);
        et_search.addTextChangedListener(textWatcher);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        //set data and list adapter
        mAdapter = new AdapterFaq(this, recyclerView, new ArrayList<Faq>());
        recyclerView.setAdapter(mAdapter);
        userPref = PreferenceManager.getDefaultSharedPreferences(ActivityFaq.this);
        loader = new LoaderView(ActivityFaq.this);

        mAdapter.setOnItemClickListener((v, obj, position) -> {
            Bundle b = new Bundle();
            Intent i;
            i = new Intent(ActivityFaq.this, ActivityFaqDetil.class);
            b.putString("tanya", obj.tanya);
            b.putString("jawab", obj.jawab);
            i.putExtras(b);
            startActivity(i);
        });
        bt_clear.setOnClickListener(view -> {
            et_search.setText("");
            searchAction();
        });

        et_search.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard();
                searchAction();
                return true;
            }
            return false;
        });
        mAdapter.setOnLoadMoreListener(current_page -> {
            if (post_total > mAdapter.getItemCount() && current_page != 0) {
                int next_page = current_page + 1;
                requestAction(next_page);
            } else {
                mAdapter.setLoaded();
            }
        });
        swipe_refresh.setOnRefreshListener(() -> {
            if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
            mAdapter.resetListData();
            requestAction(1);
        });
        progressDialog = new ProgressDialog(ActivityFaq.this);
        showNoItemView(true);
        requestAction(1);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_search.getWindowToken(), 0);
        }
    }
    private void displayApiResult(final List<Faq> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) {
            showNoItemView(true);
        }
    }
    private void searchAction() {
        query = et_search.getText().toString().trim();
        if (!query.equals("")) {
            mAdapter.resetListData();
            requestAction(1);
        } else {
            mAdapter.resetListData();
            requestAction(1);
        }
    }
    private void requestFaq(final int page_no) {
        API api = RestAdapter.createAPI();
        callbackCall = api.getFaq(page_no, Constant.REQ_BERITA, query );
        callbackCall.enqueue(new Callback<CallbackFaq>() {
            @Override
            public void onResponse(Call<CallbackFaq> call, Response<CallbackFaq> response) {
                CallbackFaq resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.faqs);
                } else {
                    onFailRequest(page_no);
                }
            }
            @Override
            public void onFailure(Call<CallbackFaq> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(() -> requestFaq(page_no), 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        findViewById(R.id.failed_retry).setOnClickListener(view -> requestAction(failed_page));
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = findViewById(R.id.lyt_no_item);
        ((TextView) findViewById(R.id.no_item_message)).setText("Data FAQ Not Found");
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(() -> swipe_refresh.setRefreshing(show));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            Intent intent = new Intent(ActivityFaq.this, MainActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        Intent intent = new Intent(ActivityFaq.this, MainActivity.class);
        startActivity(intent);
    }
}
