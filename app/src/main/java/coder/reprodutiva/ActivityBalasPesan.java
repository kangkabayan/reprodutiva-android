package coder.reprodutiva;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.EmojiTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import coder.reprodutiva.adapter.chat_adapter;
import coder.reprodutiva.data.Constant;
import coder.reprodutiva.utils.CallbackDialog;
import coder.reprodutiva.utils.DialogUtils;
import coder.reprodutiva.utils.NetworkCheck;
import coder.reprodutiva.utils.Tools;
import coder.reprodutiva.widget.AnimationsUtil;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class ActivityBalasPesan extends AppCompatActivity {
    TextView userProfileName,left_text_time;
    String no_hp,foto_profile,nama,isi_pesan,waktu;
    LinearLayout containerProfile,containerProfileInfo;
    private long Duration = 500;
    SimpleDraweeView fotona;
    EmojiTextView text_chat;
    ImageButton kirim;
    private EmojiEditText chat;
    private EmojiPopup emojiPopup;
    RecyclerView recyclerView;
    public static final int REQUEST_IMAGE = 100;
    RecyclerView.LayoutManager manager;
    Socket socket;
    ArrayList<String> chat_data=new ArrayList<>();
    ArrayList<String> pengirim_=new ArrayList<>();
    ArrayList<String> foto_pengirim_=new ArrayList<>();
    ArrayList<String> jam_kirim_ =new ArrayList<>();
    ArrayList<String> saha_ =new ArrayList<>();
    ArrayList<String> image_ =new ArrayList<>();
    ProgressDialog progressDialog = null;
    private ImageButton smileyButton;
    SharedPreferences userPref;
    private final int OPEN_IMAGE = 536;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Tools.isAndroid5()) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setStatusBarColor(Tools.getColor(this, R.color.colorPrimaryDark));
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.activity_balas_pesan);
        userPref = android.preference.PreferenceManager.getDefaultSharedPreferences(this);
        initcomponent();
        setupProgressBar();
        userProfileName.setSelected(true);
        if (Tools.isAndroid5()) {
            containerProfileInfo.post(() -> AnimationsUtil.show(containerProfileInfo, Duration));
        }
        try{
            socket= IO.socket(Constant.CHAT_SERVER_URL);
        }
        catch (URISyntaxException e){
            Log.d("error","onCreate"+e.toString());
        }
        socket.connect();
        socket.on("pesan",handling);
    }
    @Override
    public void onStart() {
        super.onStart();
        chat.addTextChangedListener(textWatcher);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.off("pesan");
        socket.disconnect();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    private Emitter.Listener handling = args -> this.runOnUiThread(() -> tambahpesan(args[0].toString(),args[1].toString(),args[2].toString(),args[3].toString(),args[4].toString(),args[5].toString()));
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            int length = charSequence.length();
            kirim.setVisibility(length > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    private void initcomponent(){
        Bundle b = getIntent().getExtras();
        assert b != null;
        no_hp = b.getString("no_hp");
        nama = b.getString("nama");
        foto_profile = b.getString("foto_profile");
        isi_pesan = b.getString("pesan");
        waktu = b.getString("waktu");
        ViewGroup rootView = findViewById(R.id.containerProfile);
        userProfileName = findViewById(R.id.left_display_name);
        if (Tools.isAndroid5()) {
            userProfileName.setTransitionName(getString(R.string.user_name_transition));
        }
        left_text_time = findViewById(R.id.left_text_time);
        left_text_time.setText(waktu);
        text_chat = findViewById(R.id.left_text);
        text_chat.setText(isi_pesan);
        userProfileName.setText(nama);
        fotona =  findViewById(R.id.left_profile_pic);
        containerProfile = findViewById(R.id.containerProfile);
        containerProfileInfo = findViewById(R.id.containerProfileInfo);
        RoundingParams circle = RoundingParams.asCircle()
                .setRoundingMethod(RoundingParams.RoundingMethod.BITMAP_ONLY);
        fotona.getHierarchy().setRoundingParams(circle);
        fotona.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
        fotona.setImageURI(Constant.getURLimgMember(foto_profile));
        containerProfile.setOnClickListener(v -> {
            if (Tools.isAndroid5())
                containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
            else
                finish();
        });
        containerProfileInfo.setOnClickListener(v -> {
            if (Tools.isAndroid5())
                containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
            else
                finish();
        });
        fotona.setOnClickListener(v -> {
            ArrayList<String> images_list = new ArrayList<>();
            images_list.add(Constant.getURLimgMember(foto_profile));
            Intent i = new Intent(ActivityBalasPesan.this, ActivityFullScreenFoto.class);
            i.putStringArrayListExtra(ActivityFullScreenFoto.EXTRA_IMGS, images_list);
            startActivity(i);
        });

        progressDialog = new ProgressDialog(this);
        smileyButton = findViewById(R.id.button_smiley);
        chat = findViewById(R.id.input_text_chat);
        kirim = findViewById(R.id.button_send);
        kirim.setOnClickListener(view -> {
            Log.e("Data","Tombol Kirim");
            if (!NetworkCheck.isConnect(this)) {
                dialogNoInternet();
            } else {
                kirim_pesan();
                chat.setText("");
            }
        });
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiPopupShownListener(() -> smileyButton.setImageResource(R.drawable.ic_keyboard)).setOnEmojiPopupDismissListener(() -> smileyButton.setImageResource(R.drawable.ic_smiley)).setOnSoftKeyboardCloseListener(() -> emojiPopup.dismiss()).build(chat);

        smileyButton.setOnClickListener(view -> emojiPopup.toggle());
        rootView.findViewById(R.id.button_add_image).setOnClickListener(view -> {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            openImageBottomSheet();
        });
    }
    private void openImageBottomSheet() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, OPEN_IMAGE);
                return;
            }
        }
        showImagePickerOptions();
    }
    public void kirim_pesan(){
        final String nama = userPref.getString("nama", "");
        final String foto = userPref.getString("foto", "");
        final String no_hpna = userPref.getString("no_hp", "");
        String pesan = chat.getText().toString().trim();
        chat.setText("");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
        String jam_kirim = mdformat.format(calendar.getTime());
        String tag_string_req = "req_kirim_chat";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Constant.URL_CHAT_GROUP, response -> {
            try {
                JSONObject jObj = new JSONObject(response);
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    tambahpesan(pesan,nama,foto,jam_kirim,no_hpna,"no.jpg");
                    socket.emit("pesan",pesan,nama,foto,jam_kirim,no_hpna,"no.jpg");
                    Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    dialogGagal();
                }
            } catch (JSONException e) {
                Log.e("Data","Nganu : " + e);
            }
        }, error -> dialogNoInternet()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("isi_pesan",pesan);
                params.put("no_hp",no_hpna);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    private void tambahpesan(String pesan, String pengirim, String foto, String jam_kirim, String saha, String image) {
        chat_data.add(pesan);
        this.pengirim_.add(pengirim);
        this.foto_pengirim_.add(foto);
        this.jam_kirim_.add(jam_kirim);
        this.saha_.add(saha);
        this.image_.add(image);
    }
    public void dialogGagal() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_info, R.string.msg_gagal_kirim_pesan, R.string.TRY_AGAIN, R.drawable.img_app_outdate, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }
            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog.show();
    }
    private void retryOpenApplication() {
        new Handler().postDelayed(() -> kirim_pesan(), 500);
    }
    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }
    private void setupProgressBar() {
        ProgressBar mProgress = findViewById(R.id.progress_bar);
        mProgress.getIndeterminateDrawable().setColorFilter(Color.parseColor("#0EC654"),
                PorterDuff.Mode.SRC_IN);
    }
    @Override
    public void onBackPressed() {
        if (Tools.isAndroid5())
            containerProfileInfo.post(() -> AnimationsUtil.hide(this, containerProfileInfo, Duration));
        else
            finish();
    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), uri);
                    new Handler().postDelayed(() -> getStringImage(bitmap), 2000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        uploadServer(encodedImage);
    }
    public void uploadServer(final String lokasi_foto) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            final String nama = userPref.getString("nama", "");
            final String foto = userPref.getString("foto", "");
            final String no_hpna = userPref.getString("no_hp", "");
            String pesan = chat.getText().toString().trim();
            chat.setText("");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Dili"));
            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");
            String jam_kirim = mdformat.format(calendar.getTime());
            Random r = new Random();
            int randomNumber = r.nextInt(555555555);
            String tag_string_req = "req_ganti_upload_foto";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    Constant.URL_UPLOAD_FOTO, response -> {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        tambahpesan("upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg");
                        socket.emit("pesan","upload_gambar",nama,foto,jam_kirim,no_hpna,randomNumber + ".jpg");
                        Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        dialogGagal();
                    }
                } catch (JSONException e) {
                    Log.e("Data","Nganu : " + e);
                    dialogGagal();
                }
            }, error -> {
                dialogNoInternet();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("foto",lokasi_foto);
                    params.put("no_hp",no_hpna);
                    params.put("nama_file",String.valueOf(randomNumber));
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
}
