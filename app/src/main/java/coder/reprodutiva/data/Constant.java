package coder.reprodutiva.data;


public class Constant {


    public static String PREF_NAME = "coder-reprodutiva.pref";
    public static String COMMON_KEY = "no_hp";
    public static String COMMON_ID = "id_user";
    public static String COMMON_NAMA = "nama_user";

    public static final int SELECT_PROFILE_PICTURE = 0x005;
    public static final int SELECT_PROFILE_CAMERA = 0x006;
    public static final int PERMISSION_REQUEST_CODE = 0x009;
    public static final String FULL_PROFILE = "fp";
    public static final String USER = "ur";

    /*public static String WEB_URL = "http://reprodutiva.coder-01.com/api_coder/";
    public static String WEB_DIR = "http://reprodutiva.coder-01.com/";*/
    public static String WEB_URL = "http://192.168.100.40/api_coder/";
    public static String WEB_DIR = "http://192.168.100.40/";

    public static String URL_LOGIN = WEB_URL + "doLogin";
    public static String URL_CEK_MEMBER = WEB_URL + "getMember";
    public static String URL_REGIS = WEB_URL + "doRegis";
    public static String URL_VERIF = WEB_URL + "doVerifikasi";
    public static String URL_SARAN = WEB_URL + "sendSaran";
    public static String URL_GANTI_FOTO = WEB_URL + "gantiFoto";
    public static String URL_UPLOAD_FOTO = WEB_URL + "uploadFoto";
    public static String URL_UPLOAD_FOTO_PRIVATE = WEB_URL + "uploadFotoPrivate";
    public static String URL_GANTI_NAMA = WEB_URL + "gantiNama";
    public static String URL_SIMPAN_MEMBER = WEB_URL + "SaveMember";
    public static String URL_CHAT = WEB_URL + "sendChat";
    public static String URL_CHAT_GROUP = WEB_URL + "sendChatGroup";

    public static String URL_GET_CHAT_GROUP = WEB_URL + "getGroupChat";
    public static String URL_GET_PRIVATE_CHAT = WEB_URL + "getPrivateChat";

    public static final String CHAT_SERVER_URL = "https://reprodutiva.appspot.com";
    public static final String BACKEND_CHAT_SERVER_URL = CHAT_SERVER_URL;

    public static final String SECURITY_CODE = "PERSIB-BANDUNG";
    public static final String APP_KEY_SECRET = "7d3d3b6c2d3683bf25bbb51533ec6dab";

    public static final boolean ENABLE_ANIMATIONS = true;

    public static int OPEN_COUNTER = 50;

    public static final String ROW_WALLPAPER = "rwppr";
    public static int LOAD_IMAGE_NOTIF_RETRY = 3;

    public static int REQ_BERITA                = 10;
    public static String getURLimgMember(String file_name) {
        return WEB_DIR + "assets/foto/user/" + file_name;
    }

    public static String getURLberita(String file_name) {
        return WEB_DIR + "assets/foto/articles/" + file_name;
    }

    public static String getURLMedia(String file_name) {
        return WEB_DIR + "assets/foto/media/" + file_name;
    }

    public static String getURLimgMenu(String file_name) {
        return WEB_DIR + "assets/foto/icon/" + file_name;
    }

}
